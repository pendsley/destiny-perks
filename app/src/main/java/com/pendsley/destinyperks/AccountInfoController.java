package com.pendsley.destinyperks;

import android.util.Log;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.pendsley.destinyperks.model.AccountSummaryWrapper;
import com.pendsley.destinyperks.model.BungieAccountData;
import com.pendsley.destinyperks.model.BungieErrorCode;
import com.pendsley.destinyperks.model.BungieResponse;
import com.pendsley.destinyperks.model.Character;
import com.pendsley.destinyperks.model.CharacterProfile;
import com.pendsley.destinyperks.model.ClassInformation;
import com.pendsley.destinyperks.model.DestinyAccount;
import com.pendsley.destinyperks.model.UserInfo;
import com.pendsley.destinyperks.network.BungieDestinyService;
import com.pendsley.destinyperks.network.BungieUserService;
import com.pendsley.destinyperks.persistence.dao.AccountOperation;
import com.pendsley.destinyperks.persistence.dao.CharacterOperation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import javax.inject.Inject;

import retrofit2.Response;

/**
 * Controller for managing Bungie account information.
 * TODO - Remove this
 *
 * @author Phil Endsley
 */
public class AccountInfoController {

    private static final String TAG = "AccountInfoController";

    private final BungieUserService userService;
    private final BungieDestinyService destinyService;
    private final AccountOperation accountOperation;
    private final CharacterOperation characterOperation;

    private final ListeningExecutorService listeningExecutorService;

    @Inject
    AccountInfoController(BungieUserService userService,
                          BungieDestinyService destinyService,
                          AccountOperation accountOperation,
                          CharacterOperation characterOperation) {
        this.userService = userService;
        this.destinyService = destinyService;
        this.accountOperation = accountOperation;
        this.characterOperation = characterOperation;

        listeningExecutorService = MoreExecutors.listeningDecorator(Executors.newSingleThreadExecutor());
    }

    public ListenableFuture retrieveCharacterInformation() {

        return listeningExecutorService.submit(
                new Runnable() {
                    @Override
                    public void run() {

                        try {
//                            Response<BungieResponse<BungieAccountData>> response = userService.getCurrentBungieAccount().execute();
                            Response<BungieResponse<BungieAccountData>> response = userService.getMembershipsForCurrentUser().execute();

                            BungieResponse<BungieAccountData> bungieResponse = response.body();


                            BungieErrorCode errorCode = BungieErrorCode.forCode(bungieResponse.getErrorCode());
                            if (BungieErrorCode.SUCCESS == errorCode) {

                                handleCharacterInformation(bungieResponse.getResponse());
                            } else {

                                Log.e(TAG, "Error: " + errorCode.name());

                            }

                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }

                    }
                }
        );
    }

    private void handleCharacterInformation(BungieAccountData accountData) {

        UserInfo userInfo = accountData.getDestinyMemberships().get(0);

        double membershipType = userInfo.getMembershipType();
        String membershipId = userInfo.getMembershipId();

        accountOperation.deleteUserInfo();
        accountOperation.deleteCharacterInfo();

        accountOperation.saveUserInfo(userInfo);

        List<Character> characters = new ArrayList<>();
        try {
            Response<BungieResponse<AccountSummaryWrapper>> accountSummaryWrapper = destinyService.getAccountSummary((int) membershipType, membershipId).execute();

            if (accountSummaryWrapper != null) {
                List<CharacterProfile> characterProfiles = accountSummaryWrapper.body().getResponse().getData().getCharacters();
                for (CharacterProfile characterProfile : characterProfiles) {
                    characters.add(fromCharacterProfile(characterProfile));
                }
            }


        accountOperation.saveCharacterInfo(membershipId, characters);
        } catch (IOException e) {

        }

    }

    private Character fromCharacterProfile(CharacterProfile characterProfile) {
        Character character = new Character();
        character.setBackgroundPath(characterProfile.getBackgroundPath());
        character.setEmblemPath(characterProfile.getEmblemPath());
        character.setCharacterId(characterProfile.getCharacterBase().getCharacterId());
        character.setCharacterClass(getClassForId(characterProfile.getCharacterBase().getClassType()));
        character.setLightLevel(Integer.valueOf(characterProfile.getCharacterBase().getLightLevel()));
        return character;
    }

    private ClassInformation getClassForId(int classType) {
        ClassInformation classInformation = new ClassInformation();
        classInformation.setClassName(characterOperation.getClassFromType(classType));
        return classInformation;
    }
}
