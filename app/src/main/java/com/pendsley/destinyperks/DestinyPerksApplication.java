package com.pendsley.destinyperks;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.pendsley.destinyperks.authentication.ReinjectFieldsEvent;
import com.pendsley.destinyperks.authentication.persistence.DestinyPerksDbHelper;
import com.pendsley.destinyperks.authentication.services.TokenRefreshReceiver;
import com.pendsley.destinyperks.inject.AccountModule;
import com.pendsley.destinyperks.inject.AppModule;
import com.pendsley.destinyperks.inject.AuthenticationComponent;
import com.pendsley.destinyperks.inject.AuthenticationModule;
import com.pendsley.destinyperks.inject.BungieApiComponent;
import com.pendsley.destinyperks.inject.DaggerAuthenticationComponent;
import com.pendsley.destinyperks.inject.DaggerBungieApiComponent;
import com.pendsley.destinyperks.inject.DatabaseModule;
import com.pendsley.destinyperks.inject.NetworkingModule;
import com.pendsley.destinyperks.persistence.dao.ManifestHelper;
import com.pendsley.destinyperks.util.BusProvider;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * The application.
 *
 * @author Phil Endsley
 */
public class DestinyPerksApplication extends Application {

    @Inject
    @Named(AuthenticationModule.REFRESH_TOKEN)
    String refreshToken;

    @Inject
    @Named(AuthenticationModule.REFRESH_TOKEN_EXPIRATION_TIME)
    Long refreshTokenExpirationTime;

    @Inject
    @Named(AuthenticationModule.ACCESS_TOKEN_EXPIRATION_TIME)
    Long accessTokenExpirationTime;

    @Inject
    BusProvider busProvider;

    private AuthenticationComponent authenticationComponent;
    private BungieApiComponent apiComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        AppModule appModule = new AppModule(this);
        AuthenticationModule authenticationModule = new AuthenticationModule();
        DatabaseModule databaseModule = new DatabaseModule(new DestinyPerksDbHelper(this),
                new ManifestHelper(this));

        NetworkingModule networkingModule = new NetworkingModule();

        authenticationComponent = DaggerAuthenticationComponent.builder()
                .authenticationModule(authenticationModule)
                .databaseModule(databaseModule)
                .networkingModule(networkingModule)
                .build();

        apiComponent = DaggerBungieApiComponent.builder()
                .appModule(appModule)
                .authenticationModule(authenticationModule)
                .databaseModule(databaseModule)
                .networkingModule(networkingModule)
                .accountModule(new AccountModule())
                .build();

        injectFields();
        busProvider.getBus().register(this);

        TokenRefreshReceiver tokenRefreshReceiver = new TokenRefreshReceiver();
        IntentFilter filter = new IntentFilter(TokenRefreshReceiver.TOKEN_REFRESH_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(tokenRefreshReceiver, filter);

        scheduleTokenRefresh();

        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                // No-op
            }

            @Override
            public void onActivityStarted(Activity activity) {
                // No-op
            }

            @Override
            public void onActivityResumed(Activity activity) {
                scheduleTokenRefresh();
            }

            @Override
            public void onActivityPaused(Activity activity) {
                // No-op
            }

            @Override
            public void onActivityStopped(Activity activity) {
                // No-op
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
                // No-op
            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                // No-op
            }
        });
    }



    @Subscribe
    public void onReinjectFields(ReinjectFieldsEvent event) {
        injectFields();

        if (event.refreshAccessTokens()) {
            scheduleTokenRefresh();
        }
    }

    public AuthenticationComponent getAuthenticationComponent() {
        return authenticationComponent;
    }

    public BungieApiComponent getApiComponent() {
        return apiComponent;
    }

    public void scheduleTokenRefresh() {

        Intent intent = new Intent();
        intent.setAction(TokenRefreshReceiver.TOKEN_REFRESH_ACTION);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void injectFields() {
        authenticationComponent.inject(this);
    }
}
