package com.pendsley.destinyperks;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;

import com.pendsley.destinyperks.authentication.ReinjectFieldsEvent;
import com.pendsley.destinyperks.persistence.dao.ManifestHelper;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

public class InitialSyncActivity extends AppCompatActivity implements ManifestDownloadController.ManifestDownloadListener,
        InventorySyncController.SyncOperationCallback {

    @Inject
    ManifestDownloadController manifestDownloadController;

    @Inject
    InventorySyncController inventorySyncController;

    private ManifestDownloadViewController manifestDownloadViewController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectFields();

        setContentView(R.layout.activity_manifest);

        manifestDownloadViewController = new ManifestDownloadViewController(this);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        startDownload();
    }

    @Override
    public void onManifestDownloadFinished(Throwable t) {
        if (t == null) {
            injectFields();
            inventorySyncController.syncCharactersAndInventory(this);
        } else {
            showErrorMessage();
        }
    }

    @Override
    public void manifestProgressChanged(final int percentFinished) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                manifestDownloadViewController.updateProgressText(getString(R.string.downloading_manifest));
                manifestDownloadViewController.updateProgress(
                        ((percentFinished == 0 ? 1 : percentFinished) / 2));

            }
        });
    }

    @Override
    public void onProgressChanged(final int percentComplete) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                manifestDownloadViewController.updateProgressText(getString(R.string.downloading_items));
                manifestDownloadViewController.updateProgress(
                        ((percentComplete == 0 ? 1 : percentComplete) / 2) + 50);

            }
        });
    }

    @Override
    public void onFinishedLoading(@Nullable Throwable t) {
        if (t == null) {
            launchInventoryActivity();
        } else {
            showErrorMessage();
        }
    }

    @Subscribe
    public void onReinjectFields(ReinjectFieldsEvent event) {
        injectFields();
    }


    public void startDownload() {
        manifestDownloadViewController.showProgressBar(true);

        manifestDownloadController.retrieveManifest(getDatabasePath(ManifestHelper.DATABASE_NAME), this);
    }

    private void showErrorMessage() {
        manifestDownloadViewController.showProgressBar(false);
    }

    private void injectFields() {
        ((DestinyPerksApplication) getApplication()).getApiComponent().inject(this);
    }

    private void launchInventoryActivity() {
        Intent intent = new Intent(this, InventoryListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}


