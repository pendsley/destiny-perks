package com.pendsley.destinyperks;

import android.animation.LayoutTransition;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.pendsley.destinyperks.authentication.ReinjectFieldsEvent;
import com.pendsley.destinyperks.model.InventoryFilterOption;
import com.pendsley.destinyperks.model.ItemClassType;
import com.pendsley.destinyperks.model.LocalInventoryItem;
import com.pendsley.destinyperks.model.TalentNodeStep;
import com.pendsley.destinyperks.network.BungieDestinyService;
import com.pendsley.destinyperks.presenter.InventoryListPresenter;
import com.pendsley.destinyperks.ui.AnimationUtil;
import com.pendsley.destinyperks.ui.DividerItemDecorator;
import com.pendsley.destinyperks.util.BusProvider;
import com.pendsley.destinyperks.viewcontroller.InventoryListViewController;
import com.pendsley.destinyperks.widget.CornerImageView;
import com.pendsley.destinyperks.widget.DownRevealMaterialSheetFab;
import com.pendsley.destinyperks.widget.SheetFloatingActionButton;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

/**
 * Fragment for displaying an item list.
 *
 * @author Phil Endsley
 */
public class InventoryListFragment extends Fragment implements InventoryListViewController,
                                                               SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "InventoryListFragment";

    private static final String STATE_ITEM_CATEGORY = "itemCategory";
    private static final String STATE_ITEM_CLASS_TYPE = "itemClassType";
    private static final String STATE_FILTER_TEXT = "filterText";
    private static final String STATE_CHARACTER_FILTER_INDEX = "characterFilterIndex";

    @Inject
    InventoryListPresenter inventoryListPresenter;

    @Inject
    BusProvider busProvider;

    private SearchView searchView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView weaponsRecyclerView;
    private WeaponsAdapter weaponsAdapter;
    private DownRevealMaterialSheetFab materialSheetFab;

    private ItemSelectedListener itemSelectedListener;

    private String itemCategory = "";
    private ItemClassType itemClassType = null;

    private String filterText = "";

    public static InventoryListFragment create(String itemCategory,
                                               ItemClassType itemClassType) {
        Bundle arguments = new Bundle(2);
        arguments.putString(STATE_ITEM_CATEGORY, itemCategory);
        if (itemClassType != null) {
            arguments.putInt(STATE_ITEM_CLASS_TYPE, itemClassType.ordinal());
        }

        InventoryListFragment inventoryFragment = new InventoryListFragment();
        inventoryFragment.setArguments(arguments);
        return inventoryFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity activity = getActivity();
        if (activity instanceof ItemSelectedListener) {
            itemSelectedListener = (ItemSelectedListener) activity;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectMembers();

        setHasOptionsMenu(true);

        Bundle arguments = getArguments();
        if (arguments != null) {
            itemCategory = arguments.getString(STATE_ITEM_CATEGORY, "");
            int itemClassTypeIndex = arguments.getInt(STATE_ITEM_CLASS_TYPE, -1);
            if (itemClassTypeIndex >= 0) {
                itemClassType = ItemClassType.values()[itemClassTypeIndex];
            } else {
                itemClassType = ItemClassType.WEAPONS;
            }
        } else {
            itemClassType = ItemClassType.WEAPONS;
        }

        if (savedInstanceState != null) {
            int classTypeIndex = savedInstanceState.getInt(STATE_ITEM_CLASS_TYPE);
            itemClassType = classTypeIndex < 0 ? null : ItemClassType.values()[classTypeIndex];

            itemCategory = savedInstanceState.getString(STATE_ITEM_CATEGORY);
            filterText = savedInstanceState.getString(STATE_FILTER_TEXT);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        inventoryListPresenter.dispose();
    }

    @Override
    public void onResume() {
        super.onResume();
        busProvider.getBus().register(this);

        getActivity().setTitle(R.string.app_name);
    }

    @Override
    public void onPause() {
        super.onPause();
        busProvider.getBus().unregister(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_inventory_weapons, container, false);

        inventoryListPresenter.setInventoryListViewController(this);

        bindSwipeRefreshLayout(root);
        bindItemList(root);
        bindFilterView(root);

        return root;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        int classTypeIndex = itemClassType == null ? -1 : itemClassType.ordinal();
        outState.putInt(STATE_ITEM_CLASS_TYPE, classTypeIndex);
        outState.putString(STATE_ITEM_CATEGORY, itemCategory);

        outState.putString(STATE_FILTER_TEXT, searchView.getQuery().toString());
        outState.putInt(STATE_CHARACTER_FILTER_INDEX, weaponsAdapter.getCharacterFilter());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState != null) {
            int characterFilterIndex = savedInstanceState.getInt(STATE_CHARACTER_FILTER_INDEX);
            weaponsAdapter.setCharacterFilter(characterFilterIndex);
        }

        inventoryListPresenter.loadCharacterEmblemPaths();
        loadInventoryItems();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_item_list, menu);

        MenuItem filterItem = menu.findItem(R.id.filter_view);
        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.filter_view));
        searchView.setQueryHint(getString(R.string.filter));
        searchView.setOnQueryTextListener(weaponsAdapter);

        LinearLayout searchBar = (LinearLayout) searchView.findViewById(R.id.search_bar);
        searchBar.setLayoutTransition(new LayoutTransition());

        MenuItemCompat.setOnActionExpandListener(filterItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {

                return true;
            }
        });

        if (!Strings.isNullOrEmpty(filterText)) {
            filterItem.expandActionView();
            searchView.setQuery(filterText, false);
        }
    }

    @Override
    public void onRefresh() {
        syncInventoryItems();
    }

    @Override
    public void displayInventoryList(List<LocalInventoryItem> inventoryItems) {
        Collections.sort(inventoryItems, new Comparator<LocalInventoryItem>() {
            @Override
            public int compare(LocalInventoryItem localInventoryItem1, LocalInventoryItem localInventoryItem2) {
                return localInventoryItem1.getName().compareTo(localInventoryItem2.getName());
            }
        });
        Collections.sort(inventoryItems, new Comparator<LocalInventoryItem>() {
            @Override
            public int compare(LocalInventoryItem localInventoryItem1, LocalInventoryItem localInventoryItem2) {
                if (localInventoryItem1.getLight() == null) {
                    return 1;
                } else if (localInventoryItem2.getLight() == null) {
                    return -1;
                }
                return localInventoryItem2.getLight().compareTo(localInventoryItem1.getLight());
            }
        });

        weaponsAdapter.setWeapons(inventoryItems);
        weaponsAdapter.notifyDataSetChanged();
        weaponsRecyclerView.startLayoutAnimation();

    }

    @Override
    public void showLoadingIndicator() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoadingIndicator() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public Context context() {
        return getContext();
    }

    @Subscribe
    public void onRefreshToken(ReinjectFieldsEvent event) {
        injectMembers();
    }

    private void injectMembers() {
        ((DestinyPerksApplication) getActivity().getApplication()).getApiComponent().inject(this);
    }

    private void bindSwipeRefreshLayout(View root) {
        swipeRefreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.refresh_container);
        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.accent);
        swipeRefreshLayout.setColorSchemeResources(R.color.icons);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void bindItemList(View root) {
        weaponsRecyclerView = (RecyclerView) root.findViewById(R.id.weapons_list);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(root.getContext());
        weaponsRecyclerView.setLayoutManager(layoutManager);
        weaponsRecyclerView.addItemDecoration(new DividerItemDecorator(root.getContext()));

        LayoutAnimationController animationController = AnimationUtil.fastFadeInLayoutAnimationController(root.getContext());
        weaponsRecyclerView.setLayoutAnimation(animationController);

        weaponsAdapter = new WeaponsAdapter(itemSelectedListener,
                                            inventoryListPresenter.loadCharacterEmblemPaths());
        weaponsRecyclerView.setAdapter(weaponsAdapter);
    }

    private void bindFilterView(View root) {
        RecyclerView filterOptionsRecyclerView = (RecyclerView) root.findViewById(R.id.sheet_list);
        RecyclerView.LayoutManager filterOptionsLayoutManager = new LinearLayoutManager(root.getContext());
        filterOptionsRecyclerView.setLayoutManager(filterOptionsLayoutManager);

        FilterOptionsAdapter filterOptionsAdapter = new FilterOptionsAdapter(inventoryListPresenter.loadFilterOptions());
        filterOptionsRecyclerView.setAdapter(filterOptionsAdapter);
        filterOptionsAdapter.notifyDataSetChanged();

        final SheetFloatingActionButton button = (SheetFloatingActionButton) root.findViewById(R.id.filter_button);
        View sheet = root.findViewById(R.id.sheet_container);
        View overlay = root.findViewById(R.id.overlay);

        @SuppressWarnings("deprecation") // Can't use because of minimum sdk
                int buttonColor = root.getResources().getColor(R.color.accent);
        @SuppressWarnings("deprecation") // Can't use because of minimum sdk
                int sheetColor = root.getResources().getColor(R.color.primary_dark);

        materialSheetFab = new DownRevealMaterialSheetFab(button, sheet, overlay, sheetColor, buttonColor);

        // This is a hack to get around a known bug
        // https://code.google.com/p/android/issues/detail?id=221387
        button.post(new Runnable() {
            @Override
            public void run() {
                button.requestLayout();
                button.setVisibility(View.VISIBLE);
            }
        });
    }

    private void loadInventoryItems() {
        if (!Strings.isNullOrEmpty(itemCategory)) {
            inventoryListPresenter.loadInventoryListByCategory(itemCategory);
        } else {
            inventoryListPresenter.loadInventoryListByClassType(itemClassType);
        }
    }

    private void syncInventoryItems() {
        if (!Strings.isNullOrEmpty(itemCategory)) {
            inventoryListPresenter.syncInventoryItemsAndLoadByCategory(itemCategory);
        } else {
            inventoryListPresenter.syncInventoryItemsAndLoadByClassType(itemClassType);
        }
    }

    private void characterFilterSelected(int characterIndex) {
        materialSheetFab.hideSheet();
        weaponsAdapter.setCharacterFilter(characterIndex);
    }

    interface ItemSelectedListener {
        void onItemSelected(LocalInventoryItem selectedItem);
    }

    private static class WeaponsAdapter extends RecyclerView.Adapter<ItemViewHolder> implements SearchView.OnQueryTextListener {

        private static int ALL_FILTER = 4;

        private final ItemSelectedListener itemSelectedListener;
        private final List<String> emblemPaths;

        private List<LocalInventoryItem> originalWeapons;
        private List<LocalInventoryItem> displayWeapons;

        private String filterText = "";
        private int characterFilter = ALL_FILTER;

        WeaponsAdapter(ItemSelectedListener itemSelectedListener,
                       List<String> emblemPaths) {

            this.itemSelectedListener = itemSelectedListener;
            this.emblemPaths = emblemPaths;

            displayWeapons = originalWeapons;
        }


        @Override
        public ItemViewHolder onCreateViewHolder(ViewGroup parent,
                                                 int viewType) {

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_weapon, parent, false);

            return new ItemViewHolder(view, itemSelectedListener);
        }

        @Override
        public void onBindViewHolder(ItemViewHolder holder,
                                     int position) {
            LocalInventoryItem item = displayWeapons.get(position);
            Integer characterIndex = item.getCharacterIndex();

            holder.bindData(item, characterIndex >= 0 ? emblemPaths.get(characterIndex) : null);
        }

        @Override
        public int getItemCount() {
            return displayWeapons == null ? 0 : displayWeapons.size();
        }

        @Override
        public boolean onQueryTextSubmit(final String query) {
            filterText = query.toLowerCase();
            filterItems();

            return true;
        }

        @Override
        public boolean onQueryTextChange(final String newText) {
            filterText = newText.toLowerCase();
            filterItems();

            return true;
        }

        void setWeapons(List<LocalInventoryItem> items) {
            this.originalWeapons = items;
            filterItems();
        }

        void setCharacterFilter(int characterIndex) {
            this.characterFilter = characterIndex;

            filterItems();
        }

        int getCharacterFilter() {
            return characterFilter;
        }

        private void filterItems() {
            if (originalWeapons == null) {
                return;
            }

            displayWeapons = Lists.newArrayList(Collections2.filter(originalWeapons, new Predicate<LocalInventoryItem>() {
                @Override
                public boolean apply(LocalInventoryItem input) {

                    if (!Strings.isNullOrEmpty(filterText)) {

                        // Each word in the search view must at least partially match the result
                        String[] allFilterCriteria = filterText.split(" ");

                        for (String filterCriteria : allFilterCriteria) {

                            boolean passes = (input.getName().toLowerCase().contains(filterCriteria) ||
                                    input.getItemType().toLowerCase().contains(filterCriteria) ||
                                    input.getItemTypeName().toLowerCase().contains(filterCriteria));


                            if (characterFilter != ALL_FILTER) {
                                passes = passes || input.getCharacterIndex() == characterFilter;
                            }

                            for (TalentNodeStep talentNodeStep : input.getTalentNodeSteps()) {
                                if (talentNodeStep.getNodeStepName().toLowerCase().contains(filterCriteria)) {
                                    passes = passes || characterFilter == ALL_FILTER ||
                                            input.getCharacterIndex() == characterFilter;
                                }
                            }

                            if (!passes) {
                                return false;
                            }

                        }

                        return true;

                    } else
                        return characterFilter == ALL_FILTER ||
                                input.getCharacterIndex() == characterFilter;
                }
            }));

            notifyDataSetChanged();

        }
    }

    private static final class ItemViewHolder extends RecyclerView.ViewHolder {

        private final View view;
        private final ItemSelectedListener itemSelectedListener;

        private final CornerImageView emblemView;
        private final TextView weaponNameView;
        private final TextView weaponTypeView;
        private final TextView lightView;


        ItemViewHolder(View itemView,
                       ItemSelectedListener itemSelectedListener) {
            super(itemView);

            this.view = itemView;
            this.itemSelectedListener = itemSelectedListener;

            emblemView = (CornerImageView) itemView.findViewById(R.id.character_emblem);
            weaponNameView = (TextView) itemView.findViewById(R.id.weapon_name_view);
            weaponTypeView = (TextView) itemView.findViewById(R.id.weapon_type_view);
            lightView = (TextView) itemView.findViewById(R.id.weapon_light);

            view.setTag(this);
        }

        void bindData(final LocalInventoryItem item,
                      String emblemPath) {

            weaponNameView.setText(item.getName());
            weaponTypeView.setText(item.getItemTypeName());
            lightView.setText(String.valueOf(item.getLight()));

            if (item.isEquipped()) {
                emblemView.setShowCorner(true);
            } else {
                emblemView.setShowCorner(false);
            }

            if (!Strings.isNullOrEmpty(emblemPath)) {
                String url = BungieDestinyService.BASE_URL + emblemPath;
                Picasso.with(view.getContext()).load(url).into(emblemView);
            } else {

                Picasso.with(view.getContext()).load(R.drawable.ic_vault_emblem).into(emblemView);
            }

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemSelectedListener.onItemSelected(item);
                }
            });
        }

    }

    private final class FilterOptionsAdapter extends RecyclerView.Adapter<FilterOptionsHolder> {

        private List<InventoryFilterOption> filterOptions;

        FilterOptionsAdapter(List<InventoryFilterOption> filterOptions) {
            this.filterOptions = filterOptions;
        }

        @Override
        public FilterOptionsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_inventory_filter_option, parent, false);

            return new FilterOptionsHolder(view);
        }

        @Override
        public void onBindViewHolder(FilterOptionsHolder holder, int position) {
            holder.bindData(filterOptions.get(position));
        }

        @Override
        public int getItemCount() {
            return filterOptions.size();
        }

    }

    private final class FilterOptionsHolder extends RecyclerView.ViewHolder {

        private final ImageView filterBackgroundView;
        private final TextView descriptionView;

        private int characterIndex;

        FilterOptionsHolder(View itemView) {
            super(itemView);

            filterBackgroundView = (ImageView) itemView.findViewById(R.id.filter_background);
            descriptionView = (TextView) itemView.findViewById(R.id.filter_description);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    characterFilterSelected(characterIndex);
                }
            });
        }

        void bindData(InventoryFilterOption inventoryFilterOption) {

            this.characterIndex = inventoryFilterOption.getCharacterIndex();

            if (characterIndex < 0) {
                // A character index less than 0 means it's in the vault
                Picasso.with(filterBackgroundView.getContext()).load(R.drawable.ic_vault_background).into(filterBackgroundView);
            } else {
                String url = BungieDestinyService.BASE_URL + inventoryFilterOption.getEmblemBackgroundPath();
                Picasso.with(filterBackgroundView.getContext()).load(url).into(filterBackgroundView);
            }

            descriptionView.setText(inventoryFilterOption.getDescription());
        }
    }
}
