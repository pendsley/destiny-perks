package com.pendsley.destinyperks;

import android.support.annotation.Nullable;
import android.util.Log;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.pendsley.destinyperks.model.AllItemsSummaryWrapper;
import com.pendsley.destinyperks.model.BungieResponse;
import com.pendsley.destinyperks.model.Item;
import com.pendsley.destinyperks.model.ItemClassType;
import com.pendsley.destinyperks.model.ItemDetailTalentNode;
import com.pendsley.destinyperks.model.ItemDetails;
import com.pendsley.destinyperks.model.ItemDetailsResponseWrapper;
import com.pendsley.destinyperks.model.LocalInventoryItem;
import com.pendsley.destinyperks.model.OnlineItem;
import com.pendsley.destinyperks.model.Stat;
import com.pendsley.destinyperks.model.TalentNode;
import com.pendsley.destinyperks.model.TalentNodeStep;
import com.pendsley.destinyperks.model.WeaponTalentNodes;
import com.pendsley.destinyperks.network.BungieDestinyService;
import com.pendsley.destinyperks.persistence.dao.AccountOperation;
import com.pendsley.destinyperks.persistence.dao.InventoryOperation;
import com.pendsley.destinyperks.persistence.dao.ItemStatsDao;
import com.pendsley.destinyperks.persistence.dao.LocalInventoryDao;
import com.pendsley.destinyperks.persistence.dao.TalentNodeDao;
import com.pendsley.destinyperks.persistence.dao.WeaponTalentOperation;
import com.pendsley.destinyperks.util.HandlerExecutor;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Inject;

import retrofit2.Response;

/**
 * Controller for syncing local item information with the Destiny servers.
 * TODO - remove
 *
 * @author Phil Endsley
 */
public class InventorySyncController {

    private static final String TAG = "InventorySync";

    private static final List<Long> WEAPON_HASHES = Lists.newArrayList(
            1498876634L, // Primary
            2465295065L, // Special
            953998645L, // Heavy
            4046403665L); // Weapons

    private static final List<Long> ARMOR_HASHES = Lists.newArrayList(
            3003523923L, // Armor
            3448274439L, // Helmet
            3551918588L, // Gauntlets
            14239492L, // Chest
            20886954L, // Legs
            1585787867L, // Class
            434908299L, // Artifact
            4023194814L); // Ghost

    private final BungieDestinyService destinyService;
    private final InventoryOperation inventoryOperation;
    private final LocalInventoryDao localInventoryDao;
    private final TalentNodeDao talentNodeDao;
    private final WeaponTalentOperation weaponTalentOperation;
    private final ItemStatsDao itemStatsDao;
    private final AccountInfoController accountInfoController;
    private final AccountOperation accountOperation;

    // We're not injecting these, since it's possible they:
    // 1. Don't exist yet
    // 2. Will change if an account sync is done first
    private int membershipType;
    private String membershipId;
    private List<String> characterIds;

    private final ListeningExecutorService executorService;


    @Inject
    InventorySyncController(BungieDestinyService destinyService,
                            InventoryOperation inventoryOperation,
                            LocalInventoryDao localInventoryDao,
                            TalentNodeDao talentNodeDao,
                            WeaponTalentOperation weaponTalentOperation,
                            ItemStatsDao itemStatsDao,
                            AccountInfoController accountInfoController,
                            AccountOperation accountOperation) {
        this.destinyService = destinyService;
        this.inventoryOperation = inventoryOperation;
        this.localInventoryDao = localInventoryDao;
        this.talentNodeDao = talentNodeDao;
        this.weaponTalentOperation = weaponTalentOperation;
        this.itemStatsDao = itemStatsDao;
        this.accountInfoController = accountInfoController;
        this.accountOperation = accountOperation;
        this.characterIds = Lists.newArrayList();

        executorService = MoreExecutors.listeningDecorator(Executors.newSingleThreadScheduledExecutor());
    }

    public void syncCharactersAndInventory(final SyncOperationCallback syncOperationCallback) {

        final ListenableFuture accountFuture = accountInfoController.retrieveCharacterInformation();
        accountFuture.addListener(new Runnable() {
            @Override
            public void run() {
                membershipId = accountOperation.getMembershipId();
                characterIds = accountOperation.getCharacterIds(membershipId);
                syncInventory(syncOperationCallback);
            }
        }, new HandlerExecutor());

    }

    public void syncInventory(final SyncOperationCallback syncOperationCallback) {
        final ListenableFuture future = retrieveVaultItemIds(syncOperationCallback);

        future.addListener(new Runnable() {
            @Override
            public void run() {
                try {
                    future.get();

                    syncOperationCallback.onFinishedLoading(null);

                } catch (ExecutionException | InterruptedException e) {
                    syncOperationCallback.onFinishedLoading(e);
                }
            }
        }, new HandlerExecutor());
    }

    private ListenableFuture retrieveVaultItemIds(final SyncOperationCallback syncOperationCallback) {

        return executorService.submit(new Runnable() {
            @Override
            public void run() {

                try {
                    membershipId = accountOperation.getMembershipId();
                    membershipType = accountOperation.getMembershipType(membershipId).intValue();

                    Response<BungieResponse<AllItemsSummaryWrapper>> response = destinyService.getAllItemsSummary(membershipType, membershipId).execute();

                    AllItemsSummaryWrapper wrapper = response.body().getResponse();

                    if (wrapper == null) {
                        Log.e(TAG, "Error: " + response.code() + " - " + response.message());
                        return;
                    }

                    List<OnlineItem> onlineItems = Lists.newArrayList();

                    for (OnlineItem item : wrapper.getData().getItems()) {
                        Long bucketHash = item.getBucketHash();
                        if (WEAPON_HASHES.contains(bucketHash)) {
                            item.setItemClassType(ItemClassType.WEAPONS);
                            onlineItems.add(item);
                        } else if (ARMOR_HASHES.contains(bucketHash)) {
                            item.setItemClassType(ItemClassType.ARMOR);
                            onlineItems.add(item);
                        }
                    }

                    retrieveItemDetails(onlineItems, syncOperationCallback);

                } catch (Throwable e) {
                    Log.e(TAG, "Error: ", e);
                }
            }
        });
    }

    private void retrieveItemDetails(List<OnlineItem> onlineItems,
                                     final SyncOperationCallback syncOperationCallback) {
        if (onlineItems.isEmpty()) {
            return;
        }

        final int totalItems = onlineItems.size();

        final CountDownLatch countDownLatch = new CountDownLatch(onlineItems.size());
        final CountDownLatch syncingCountdownLatch = new CountDownLatch(onlineItems.size());

        ExecutorService executorService = Executors.newCachedThreadPool();

        // Collect all the details in memory, then batch insert/delete them. This is for performance
        final Set<Item> itemSet = Collections.newSetFromMap(new ConcurrentHashMap<Item, Boolean>());
        final ConcurrentHashMap<String, List<TalentNodeStep>> talentNodeStepsMap = new ConcurrentHashMap<>();
        final ConcurrentHashMap<String, List<Stat>> statsMap = new ConcurrentHashMap<>();
        final Set<String> itemsToDelete = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
        final Map<String, LocalInventoryItem> allCurrentLocalItems = localInventoryDao.getAllLocalItemsMap();

        Log.d(TAG, "Retrieving details for " + onlineItems.size() + " items");
        for (final OnlineItem onlineItem : onlineItems) {
            executorService.submit(new Runnable() {
                @Override
                public void run() {

                    try {
                        String characterId;
                        int index = onlineItem.getCharacterIndex();

                        if (index < 0) {
                            characterId = characterIds.get(0);
                        } else {
                            characterId = characterIds.get(index);
                        }

                        Response<BungieResponse<ItemDetailsResponseWrapper>> response = destinyService.getItemDetail(membershipType,
                                membershipId,
                                characterId,
                                onlineItem.getItemId()).execute();

                        ItemDetailsResponseWrapper wrapper = response.body().getResponse();
                        if (wrapper == null) {
                            return;
                        }

                        Item item = inventoryOperation.getItem(onlineItem.getItemHash());

                        ItemDetails onlineItemDetails = wrapper.getData().getItem();

                        item.setItemInstanceId(onlineItem.getItemId());
                        item.setCharacterIndex(onlineItem.getCharacterIndex());
                        item.setItemClass(onlineItem.getItemClassType());
                        item.setEquipped(onlineItemDetails.getEquipped());

                        Stat primaryStat = onlineItemDetails.getPrimaryStat();
                        if (primaryStat != null) {
                            item.setLight(primaryStat.getValue());
                        }

                        LocalInventoryItem oldLocalItem = allCurrentLocalItems.get(item.getItemInstanceId());

                        if (oldLocalItem != null) {

                            allCurrentLocalItems.remove(oldLocalItem.getInstanceId());

                            boolean same = oldLocalItem.getCharacterIndex().equals(item.getCharacterIndex()) &&
                                    oldLocalItem.getLight().equals(item.getLight()) &&
                                    oldLocalItem.isEquipped().equals(item.getEquipped());

                            // If there's no difference, saving details will create a duplicate entry
                            if (same) {
                                return;
                            }

                            // If there is a difference, we need to delete the existing details
                            // before we insert the updated ones
                            itemsToDelete.add(item.getItemInstanceId());

                        }

                        itemSet.add(item);

                        List<TalentNodeStep> talentNodeSteps = getTalentNodeSteps(wrapper.getData().getTalentNodes(),
                                item.getTalentGridHash());

                        if (!talentNodeSteps.isEmpty()) {
                            talentNodeStepsMap.put(onlineItem.getItemId(), talentNodeSteps);
                        }

                        List<Stat> itemStats = onlineItemDetails.getStats();
                        statsMap.put (item.getItemInstanceId(), itemStats);

                    } catch (IOException e) {
                        Log.e(TAG, "Error getting item details", e);

                    } finally {
                        countDownLatch.countDown();

                        final long currentItem = countDownLatch.getCount();
                        new HandlerExecutor().execute(new Runnable() {
                            @Override
                            public void run() {
                                syncOperationCallback.onProgressChanged((int) (((totalItems - currentItem) / (float) totalItems) * 100));
                            }
                        });

                        if (currentItem == 0) {
                            handleItemDetailsPersistence(allCurrentLocalItems,
                                    itemSet,
                                    talentNodeStepsMap,
                                    statsMap,
                                    itemsToDelete);
                            syncingCountdownLatch.countDown();
                        } else {
                            syncingCountdownLatch.countDown();
                        }
                    }
                }
            });

        }

        try {

            // We use two countdown latches. countDownLatch counts down every time we process an item.
            // syncingCountdownLatch counts down after every operation in the finally block is ocmpleted.
            // We wait for syncingCountdownLatch to finish so we can guarantee the database writes/
            // deletes are finished before we return. This is hacky, but the quickest way I could
            // think of to do this.
            syncingCountdownLatch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }


    private List<TalentNodeStep> getTalentNodeSteps(List<ItemDetailTalentNode> instanceTalentNodes,
                                                    Long talentGridHash) {

        WeaponTalentNodes dbTalentNodes = weaponTalentOperation.getWeaponTalentNodes(talentGridHash);

        if (dbTalentNodes == null) {
            return Lists.newArrayList();
        }

        Map<Integer, TalentNode> dbTalentNodeMap = Maps.newHashMap();
        for (TalentNode talentNode : dbTalentNodes.getNodes()) {
            dbTalentNodeMap.put(talentNode.getNodeHash(), talentNode);
        }

        List<TalentNodeStep> talentNodeSteps = Lists.newArrayList();

        for (ItemDetailTalentNode itemDetailTalentNode : instanceTalentNodes) {
            if (itemDetailTalentNode.isHidden()) {
                continue;
            }

            TalentNode talentNode = dbTalentNodeMap.get(itemDetailTalentNode.getNodeHash());

            // This isn't how this field is supposed to be used. If a node has prerequisite nodes,
            // it will replace the prerequisite node once the prerequisite node has been completed.
            // This appears to only be on items with ornaments. Since we don't indicate which talent
            // nodes are selected or completed, we can get away with doing this quick and dirty
            // filtering.
            List<Integer> prerequisiteNodes = talentNode.getPrerequisiteNodeIndexes();
            if (prerequisiteNodes != null && !prerequisiteNodes.isEmpty()) {
                continue;
            }
            TalentNodeStep talentNodeStep = talentNode.getSteps().get(itemDetailTalentNode.getStepIndex());
            talentNodeStep.setRow(talentNode.getRow());
            talentNodeStep.setColumn(talentNode.getColumn());
            talentNodeSteps.add(talentNodeStep);
        }

        Collections.reverse(talentNodeSteps);

        return talentNodeSteps;
    }


    private void handleItemDetailsPersistence(Map<String, LocalInventoryItem> allCurrentLocalItems,
                                              Set<Item> itemSet,
                                              ConcurrentHashMap<String, List<TalentNodeStep>> talentNodeStepsMap,
                                              ConcurrentHashMap<String, List<Stat>> statsMap,
                                              Set<String> itemsToDelete) {

        itemsToDelete.addAll(allCurrentLocalItems.keySet());

        if (!itemsToDelete.isEmpty()) {
            localInventoryDao.deleteItems(itemsToDelete);
            talentNodeDao.deleteTalentNodes(itemsToDelete);
            itemStatsDao.deleteStats(itemsToDelete);
        }

        localInventoryDao.batchInsertItems(itemSet);
        talentNodeDao.batchInsertTalentNodes(talentNodeStepsMap);
        itemStatsDao.batchInsertStats(statsMap);
    }

    public interface SyncOperationCallback {

        void onProgressChanged(int percentComplete);

        void onFinishedLoading(@Nullable Throwable t);
    }
}
