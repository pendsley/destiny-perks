package com.pendsley.destinyperks;

import android.util.Log;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.pendsley.destinyperks.model.BungieErrorCode;
import com.pendsley.destinyperks.model.BungieResponse;
import com.pendsley.destinyperks.model.Manifest;
import com.pendsley.destinyperks.network.BungieDestinyService;
import com.pendsley.destinyperks.persistence.dao.ManifestOperation;
import com.pendsley.destinyperks.util.HandlerExecutor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Controller for downloading the Destiny Manifest.
 *
 * @author Phil Endsley
 */
public class ManifestDownloadController {

    private static final String TAG = "ManifestDownload";

    private final BungieDestinyService destinyService;
    private final ManifestOperation manifestOperation;

    @Inject
    ManifestDownloadController(BungieDestinyService destinyService,
                               ManifestOperation manifestOperation) {
        this.destinyService = destinyService;
        this.manifestOperation = manifestOperation;
    }

    public void retrieveManifest(final File manifestDatabaseLocation,
                                 final ManifestDownloadListener manifestDownloadListener) {

        destinyService.getManifest().enqueue(new Callback<BungieResponse<Manifest>>() {
            @Override
            public void onResponse(Call<BungieResponse<Manifest>> call, Response<BungieResponse<Manifest>> response) {

                BungieResponse<Manifest> bungieResponse = response.body();

                BungieErrorCode errorCode = BungieErrorCode.forCode(bungieResponse.getErrorCode());

                if (BungieErrorCode.SUCCESS == errorCode) {
                    Manifest manifest = bungieResponse.getResponse();

                    handleManifestResult(manifest, manifestDatabaseLocation, manifestDownloadListener);
                } else {
                    manifestDownloadListener.onManifestDownloadFinished(new RuntimeException(errorCode.getErrorMessage()));
                }
            }

            @Override
            public void onFailure(Call<BungieResponse<Manifest>> call, Throwable t) {
                Log.e(TAG, "Error while getting manifest details", t);
                manifestDownloadListener.onManifestDownloadFinished(t);
            }
        });
    }

    private void handleManifestResult(Manifest manifest,
                                      File manifestDatabaseLocation,
                                      final ManifestDownloadListener manifestDownloadListener) {

        // See if the manifest has changed compared to our previously downloaded version
        boolean manifestUpToDate = isManifestUpToDate(manifest.getVersion());

        if (!manifestUpToDate) {
            manifestOperation.deleteManifestInfo();
            manifestOperation.insertManifestInfo(manifest.getVersion());
            final ListenableFuture downloadFuture = downloadManifest(manifest.getMobileWorldContentPaths().getEn(),
                    manifestDatabaseLocation,
                    manifestDownloadListener);

            downloadFuture.addListener(new Runnable() {
                @Override
                public void run() {
                    try {
                        downloadFuture.get();
                        manifestDownloadListener.onManifestDownloadFinished(null);
                    } catch (ExecutionException | InterruptedException e) {
                        manifestDownloadListener.onManifestDownloadFinished(e);
                    }
                }
            }, new HandlerExecutor());

            return;
        }

        manifestDownloadListener.onManifestDownloadFinished(null);

    }

    private boolean isManifestUpToDate(String remoteManifestVersion) {

        String localManifestVersion = manifestOperation.getLocalManifestVersion();

        return remoteManifestVersion.equals(localManifestVersion);
    }

    private ListenableFuture downloadManifest(final String relativeUrl,
                                              final File manifestDatabaseLocation,
                                              final ManifestDownloadListener manifestDownloadListener) {

        ListeningExecutorService downloadService = MoreExecutors.listeningDecorator(Executors.newSingleThreadExecutor());

        return downloadService.submit(new Runnable() {

            @Override
            public void run() {

                try {
                    updateProgress(manifestDownloadListener, 0);
                    URL url = new URL("https://www.bungie.net" + relativeUrl);
                    URLConnection connection = url.openConnection();

                    connection.connect();

                    ZipInputStream inputStream = new ZipInputStream(url.openStream());
                    ZipEntry entry = inputStream.getNextEntry();
                    long fileLength = entry.getSize();

                    OutputStream outputStream = new FileOutputStream(manifestDatabaseLocation);

                    byte data[] = new byte[1024];
                    int currentLength;
                    float total = 0;
                    while ((currentLength = inputStream.read(data)) != -1) {
                        total += currentLength;
                        updateProgress(manifestDownloadListener, (int)((total / fileLength) * 100));
                        outputStream.write(data, 0, currentLength);

                    }

                    outputStream.flush();
                    outputStream.close();
                    inputStream.close();
                } catch (IOException e) {
                    // Log and rethrow, so we fail appropriately
                    Log.e(TAG, e.getLocalizedMessage(), e);
                    throw new RuntimeException(e);
                }

            }
        });

    }

    private void updateProgress(final ManifestDownloadListener manifestDownloadListener,
                                final int progress) {
        new HandlerExecutor().execute(new Runnable() {
            @Override
            public void run() {
                manifestDownloadListener.manifestProgressChanged(progress);
            }
        });
    }

    public interface ManifestDownloadListener {

        void manifestProgressChanged(int percentFinished);

        void onManifestDownloadFinished(Throwable t);
    }
}
