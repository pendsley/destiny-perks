package com.pendsley.destinyperks;

import android.graphics.PorterDuff;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * View controller for downloading the manifest.
 *
 * @author Phil Endsleoy
 */
public class ManifestDownloadViewController {

    private final ProgressBar progressBar;
    private final TextView progressText;
    private final TextView errorMessageView;
    private final Button retryButton;

    public ManifestDownloadViewController(final InitialSyncActivity initialSyncActivity) {

        progressBar = (ProgressBar) initialSyncActivity.findViewById(R.id.manifest_progress_bar);
        progressBar.setIndeterminate(false);
        progressBar.getProgressDrawable().setColorFilter(initialSyncActivity.getResources().getColor(R.color.accent),
                PorterDuff.Mode.SRC_ATOP);
        progressText = (TextView) initialSyncActivity.findViewById(R.id.progress_text);
        errorMessageView = (TextView) initialSyncActivity.findViewById(R.id.error);
        retryButton = (Button) initialSyncActivity.findViewById(R.id.retry_button);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initialSyncActivity.startDownload();
            }
        });
    }

    public void showProgressBar(boolean showProgressBar) {
        if (showProgressBar) {
            progressBar.setVisibility(View.VISIBLE);
            progressText.setVisibility(View.VISIBLE);
            errorMessageView.setVisibility(View.GONE);
            retryButton.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            progressText.setVisibility(View.GONE);
            errorMessageView.setVisibility(View.VISIBLE);
            retryButton.setVisibility(View.VISIBLE);
        }
    }

    public void updateProgress(int percentComplete) {
        progressBar.setProgress(percentComplete);
    }

    public void updateProgressText(String text) {
        progressText.setText(text);
    }
}
