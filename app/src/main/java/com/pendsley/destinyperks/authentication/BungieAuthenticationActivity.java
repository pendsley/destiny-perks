package com.pendsley.destinyperks.authentication;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.pendsley.destinyperks.util.BusProvider;
import com.pendsley.destinyperks.DestinyPerksApplication;
import com.pendsley.destinyperks.InitialSyncActivity;
import com.pendsley.destinyperks.R;
import com.pendsley.destinyperks.authentication.model.AuthorizationCodeContainer;
import com.pendsley.destinyperks.authentication.model.BungieTokenContainer;
import com.pendsley.destinyperks.authentication.model.RefreshTokenContainer;
import com.pendsley.destinyperks.authentication.model.TokenContainer;
import com.pendsley.destinyperks.authentication.persistence.operation.AuthenticationTokenOperation;
import com.pendsley.destinyperks.authentication.persistence.operation.SaveTokenOperation;
import com.pendsley.destinyperks.inject.AuthenticationModule;
import com.pendsley.destinyperks.model.BungieErrorCode;
import com.pendsley.destinyperks.model.BungieResponse;
import com.squareup.otto.Subscribe;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Activity to authenticate the user.
 *
 * @author Phil Endsley
 */
public class BungieAuthenticationActivity extends AppCompatActivity {

    private static final String TAG = "BungieAuthActivity";

    private static final String AUTHORIZATION_REPOSNSE_HOST = "authorization";

    @Inject
    BungieAuthenticationService authenticationService;

    @Inject
    SaveTokenOperation saveTokenOperation;

    @Inject
    AuthenticationTokenOperation authenticationTokenOperation;

    @Inject
    @Named(AuthenticationModule.REFRESH_TOKEN)
    String refreshToken;

    @Inject
    @Named(AuthenticationModule.AUTHENTICATED)
    Boolean authenticated;

    @Inject
    BusProvider busProvider;

    private LinearLayout container;
    private WebView authWebView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_authentication);

        injectFields();

        container = (LinearLayout) findViewById(R.id.authentication_container);
        authWebView = (WebView) findViewById(R.id.auth_web_view);
        progressBar = (ProgressBar) findViewById(R.id.auth_progress_bar);

        authWebView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        authWebView.getSettings().setLoadWithOverviewMode(true);

        // Upon successful authentication and app authorization, the web view will redirect
        // to a hardcoded host. If that happens, intercept and handle the response.
        authWebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                Uri uri = Uri.parse(url);
                String host = uri.getHost();
                if (AUTHORIZATION_REPOSNSE_HOST.equals(host)) {
                    handleAuthenticationResponse(uri);
                }
            }
        });

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        if (authenticated) {
            launchManifestActivity();
            return;
        }

        Uri incomingUri = getIntent().getData();

        if (incomingUri == null && !authenticated) {
            startAuthProcess();
        } else if (!authenticated) {
            handleAuthenticationResponse(incomingUri);
        }
    }

    @Subscribe
    public void onReinjectfields(ReinjectFieldsEvent event) {
        injectFields();

        if (authenticated) {
            launchManifestActivity();
        } else {
            startAuthThroughWebView();
        }
    }

    private void startAuthProcess() {

        Long expirationTime = authenticationTokenOperation.getTokenExpirationTimeByType("Refresh");

        if (expirationTime > 0 && new Date().getTime() < expirationTime) {
            refreshAccessToken();
        } else {
            startAuthThroughWebView();
        }
    }

    private void refreshAccessToken() {

        authenticationService.getAccessTokensFromRefreshtoken(new RefreshTokenContainer(refreshToken)).enqueue(new Callback<BungieResponse<BungieTokenContainer>>() {
            @Override
            public void onResponse(Call<BungieResponse<BungieTokenContainer>> call, Response<BungieResponse<BungieTokenContainer>> response) {
                BungieResponse<BungieTokenContainer> bungieResponse = response.body();

                BungieErrorCode errorCode = BungieErrorCode.forCode(bungieResponse.getErrorCode());

                if (BungieErrorCode.SUCCESS == errorCode) {
                    handleTokenResponse(bungieResponse.getResponse());
                } else {
                    startAuthThroughWebView();
                }
            }

            @Override
            public void onFailure(Call<BungieResponse<BungieTokenContainer>> call, Throwable t) {
                Log.e(TAG, "Error refreshing access token", t);
                startAuthThroughWebView();
            }
        });
    }

    // This is Bungie's auth page. We can trust it to not hurt us
    @SuppressLint("SetJavaScriptEnabled")
    private void startAuthThroughWebView() {

        progressBar.setVisibility(View.GONE);
        authWebView.setVisibility(View.VISIBLE);

        authWebView.getSettings().setJavaScriptEnabled(true);

        authWebView.loadUrl("https://www.bungie.net/en/Application/Authorize/4154");
    }

    private void handleAuthenticationResponse(Uri incomingUri) {
        authWebView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        String authorizationCode = incomingUri.getQueryParameter("code");

        retrieveBungieAccessToken(authorizationCode);
    }

    private void retrieveBungieAccessToken(final String authorizationCode) {

        authenticationService.getAccessTokens(new AuthorizationCodeContainer(authorizationCode))
                .enqueue(new Callback<BungieResponse<BungieTokenContainer>>() {
                    @Override
                    public void onResponse(Call<BungieResponse<BungieTokenContainer>> call, Response<BungieResponse<BungieTokenContainer>> response) {

                        BungieResponse<BungieTokenContainer> bungieResponse = response.body();

                        BungieTokenContainer tokenContainer = bungieResponse.getResponse();
                        if (tokenContainer != null) {
                            handleTokenResponse(tokenContainer);
                            return;
                        }

                        BungieErrorCode errorCode = BungieErrorCode.forCode(bungieResponse.getErrorCode());

                        progressBar.setVisibility(View.INVISIBLE);

                        Log.e(TAG, "Error getting access tokens. " + errorCode);
                        showErrorSnackbar();
                    }

                    @Override
                    public void onFailure(Call<BungieResponse<BungieTokenContainer>> call, Throwable t) {
                        Log.e(TAG, t.getLocalizedMessage(), t);
                        showErrorSnackbar();
                    }
                });
    }

    private void showErrorSnackbar() {
        @SuppressWarnings("deprecation") // API requirements
        final Snackbar snackbar = Snackbar.make(container,
                R.string.auth_error,
                Snackbar.LENGTH_INDEFINITE)
                .setActionTextColor(getResources().getColor(R.color.secondary_text));

        snackbar.setAction(R.string.dismiss, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });

        snackbar.show();
    }

    private void handleTokenResponse(BungieTokenContainer tokenContainer) {

        TokenContainer accessTokenContainer = tokenContainer.getAccessToken();
        accessTokenContainer.setType("Access");

        TokenContainer refreshTokenContainer = tokenContainer.getRefreshToken();
        refreshTokenContainer.setType("Refresh");

        saveTokenOperation.deleteTokens();
        saveTokenOperation.insert(accessTokenContainer);
        saveTokenOperation.insert(refreshTokenContainer);

        busProvider.getBus().post(new ReinjectFieldsEvent(true));

        launchManifestActivity();
    }

    private void injectFields() {

        ((DestinyPerksApplication) getApplication()).getAuthenticationComponent().inject(this);
    }

    private void launchManifestActivity() {
        Intent intent = new Intent(this, InitialSyncActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

}
