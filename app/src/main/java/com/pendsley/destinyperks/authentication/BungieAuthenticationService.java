package com.pendsley.destinyperks.authentication;

import com.pendsley.destinyperks.authentication.model.AuthorizationCodeContainer;
import com.pendsley.destinyperks.authentication.model.BungieTokenContainer;
import com.pendsley.destinyperks.authentication.model.RefreshTokenContainer;
import com.pendsley.destinyperks.model.BungieResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * TODO
 */
public interface BungieAuthenticationService {

    @POST("GetAccessTokensFromCode/")
    Call<BungieResponse<BungieTokenContainer>> getAccessTokens(@Body AuthorizationCodeContainer authorizationCode);

    @POST("GetAccessTokensFromRefreshToken/")
    Call<BungieResponse<BungieTokenContainer>> getAccessTokensFromRefreshtoken(@Body RefreshTokenContainer refreshTokenContainer);
}
