package com.pendsley.destinyperks.authentication.model;

/**
 * TODO
 */
public class AuthorizationCodeContainer {

    private final String code;


    public AuthorizationCodeContainer(String authorizationCode) {
        this.code = authorizationCode;
    }

    public String getCode() {
        return code;
    }
}
