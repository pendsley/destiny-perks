package com.pendsley.destinyperks.authentication.model;

/**
 * TODO
 */
public class BungieTokenContainer {

    private TokenContainer accessToken;
    private TokenContainer refreshToken;
    private Integer scope;

    public TokenContainer getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(TokenContainer accessToken) {
        this.accessToken = accessToken;
    }

    public TokenContainer getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(TokenContainer refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Integer getScope() {
        return scope;
    }

    public void setScope(Integer scope) {
        this.scope = scope;
    }
}
