package com.pendsley.destinyperks.authentication.persistence.operation;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;

import com.pendsley.destinyperks.inject.DatabaseModule;
import com.pendsley.destinyperks.authentication.persistence.TokenContract;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * TODO
 */
public class AuthenticationTokenOperation {

    private static final String SELECTION = TokenContract.TokenEntry.COLUMN_NAME_TYPE + " = ?";

    private final SQLiteDatabase database;

    @Inject
    public AuthenticationTokenOperation(@Named(DatabaseModule.READABLE_DATABASE) SQLiteDatabase database) {
        this.database = database;
    }

    public String getTokenByType(String tokenType) {

        try (Cursor cursor = database.query(TokenContract.TokenEntry.TABLE_NAME,
                new String[] { TokenContract.TokenEntry.COLUMN_NAME_TOKEN },
                SELECTION,
                new String[] { tokenType },
                null,
                null,
                null)) {

            String token = "";

            if (cursor.moveToNext()) {
                token = cursor.getString(cursor.getColumnIndexOrThrow(TokenContract.TokenEntry.COLUMN_NAME_TOKEN));
            }

            return token;
        }
    }

    public Long getTokenExpirationTimeByType(String tokenType) {

        try (Cursor cursor = database.query(TokenContract.TokenEntry.TABLE_NAME,
                new String[] { TokenContract.TokenEntry.COLUMN_NAME_EXPIRES },
                SELECTION,
                new String[] { tokenType },
                null,
                null,
                null)) {

            Long tokenExpiration = -1L;

            if (cursor.moveToNext()) {
                tokenExpiration = cursor.getLong(cursor.getColumnIndexOrThrow(TokenContract.TokenEntry.COLUMN_NAME_EXPIRES));
            }

            return tokenExpiration;
        }
    }
}
