package com.pendsley.destinyperks.authentication.persistence.operation;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.pendsley.destinyperks.inject.DatabaseModule;
import com.pendsley.destinyperks.authentication.model.TokenContainer;
import com.pendsley.destinyperks.authentication.persistence.TokenContract;
import com.pendsley.destinyperks.persistence.dao.InsertOperation;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * TODO
 */
public class SaveTokenOperation implements InsertOperation<TokenContainer> {

    private final SQLiteDatabase database;

    @Inject
    public SaveTokenOperation(@Named(DatabaseModule.WRITABLE_DATABASE) SQLiteDatabase database) {
        this.database = database;
    }

    @Override
    public void insert(TokenContainer tokenContainer) {

        long expires = new Date().getTime();
        expires += TimeUnit.MILLISECONDS.convert(tokenContainer.getExpires(), TimeUnit.SECONDS);
        expires -= TimeUnit.MILLISECONDS.convert(10, TimeUnit.MINUTES);

        ContentValues values = new ContentValues(3);
        values.put(TokenContract.TokenEntry.COLUMN_NAME_TYPE, tokenContainer.getType());
        values.put(TokenContract.TokenEntry.COLUMN_NAME_TOKEN, tokenContainer.getValue());
        values.put(TokenContract.TokenEntry.COLUMN_NAME_EXPIRES, expires);

        database.insert(TokenContract.TokenEntry.TABLE_NAME, null, values);
    }

    public void updateToken(TokenContainer tokenContainer) {

        String whereClause = TokenContract.TokenEntry.COLUMN_NAME_TYPE + " = ?";

        long expires = new Date().getTime();
        expires += TimeUnit.MILLISECONDS.convert(tokenContainer.getExpires(), TimeUnit.SECONDS);
        expires -= TimeUnit.MILLISECONDS.convert(10, TimeUnit.MINUTES);

        ContentValues values = new ContentValues();
        values.put(TokenContract.TokenEntry.COLUMN_NAME_TYPE, tokenContainer.getType());
        values.put(TokenContract.TokenEntry.COLUMN_NAME_TOKEN, tokenContainer.getValue());
        values.put(TokenContract.TokenEntry.COLUMN_NAME_EXPIRES, expires);

        database.update(TokenContract.TokenEntry.TABLE_NAME,
                values,
                whereClause,
                new String[] { tokenContainer.getType() });
    }

    public void deleteTokens() {

        database.delete(TokenContract.TokenEntry.TABLE_NAME, null, null);
    }
}
