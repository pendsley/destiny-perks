package com.pendsley.destinyperks.inject;

import com.pendsley.destinyperks.DestinyPerksApplication;
import com.pendsley.destinyperks.authentication.BungieAuthenticationActivity;
import com.pendsley.destinyperks.authentication.services.TokenRefreshReceiver;
import com.pendsley.destinyperks.login.LoginActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * TODO
 */
@Singleton
@Component(modules = {DatabaseModule.class,
        AuthenticationModule.class,
        NetworkingModule.class})
public interface AuthenticationComponent {

    void inject(DestinyPerksApplication destinyPerksApplication);

    void inject(LoginActivity loginActivity);

    void inject(BungieAuthenticationActivity bungieAuthenticationActivity);

    void inject(TokenRefreshReceiver tokenRefreshReceiver);
}
