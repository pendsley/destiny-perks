package com.pendsley.destinyperks.inject;

import com.pendsley.destinyperks.network.BungieApiHeaderInterceptor;
import com.pendsley.destinyperks.authentication.persistence.operation.AuthenticationTokenOperation;

import java.util.Date;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * TODO
 */
@Module
public class AuthenticationModule {

    public static final String ACCESS_TOKEN = "accessToken";
    public static final String ACCESS_TOKEN_EXPIRATION_TIME = "accessTokenExpirationTime";

    public static final String REFRESH_TOKEN = "refreshToken";
    public static final String REFRESH_TOKEN_EXPIRATION_TIME = "refreshTokenExpirationTime";

    public static final String AUTHENTICATED = "authenticated";

    @Provides
    @Named(ACCESS_TOKEN)
    String provideAccessToken(AuthenticationTokenOperation authenticationTokenOperation) {
        return authenticationTokenOperation.getTokenByType("Access");
    }

    @Provides
    @Named(ACCESS_TOKEN_EXPIRATION_TIME)
    Long provideAccessTokenExpirationTime(AuthenticationTokenOperation authenticationTokenOperation) {
        return authenticationTokenOperation.getTokenExpirationTimeByType("Access");
    }

    @Provides
    @Named(REFRESH_TOKEN)
    String provideRefreshToken(AuthenticationTokenOperation authenticationTokenOperation) {
        return authenticationTokenOperation.getTokenByType("Refresh");
    }

    @Provides
    @Named(REFRESH_TOKEN_EXPIRATION_TIME)
    Long provideRefreshTokenExpirationTime(AuthenticationTokenOperation authenticationTokenOperation) {
        return authenticationTokenOperation.getTokenExpirationTimeByType("Refresh");
    }

    @Provides
    BungieApiHeaderInterceptor provideApiHeaderInterceptor(@Named(ACCESS_TOKEN) String accesstoken,
                                                           @Named(ACCESS_TOKEN_EXPIRATION_TIME) Long accessTokenExpiration) {
        return new BungieApiHeaderInterceptor(accesstoken, accessTokenExpiration);
    }

    @Provides
    @Named(AUTHENTICATED)
    Boolean provideAuthenticatd(@Named(ACCESS_TOKEN) String accessToken,
                                AuthenticationTokenOperation authenticationTokenOperation) {
        long expirationTime = authenticationTokenOperation.getTokenExpirationTimeByType("Access");
        return new Date().getTime() < expirationTime &&
                accessToken != null && !"".equals(accessToken);
    }
}
