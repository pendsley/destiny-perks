package com.pendsley.destinyperks.inject;

import com.pendsley.destinyperks.EditLoadoutActivity;
import com.pendsley.destinyperks.InitialSyncActivity;
import com.pendsley.destinyperks.InventoryListActivity;
import com.pendsley.destinyperks.InventoryListFragment;
import com.pendsley.destinyperks.LoadoutDetailsFragment;
import com.pendsley.destinyperks.LoadoutsListFragment;
import com.pendsley.destinyperks.ItemDetailsFragment;
import com.pendsley.destinyperks.NewLoadoutSlotSelectionFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * TOOD
 */
@Singleton
@Component(modules = { NetworkingModule.class,
        AuthenticationModule.class,
        DatabaseModule.class,
        AccountModule.class,
        AppModule.class})
public interface BungieApiComponent {

    void inject(InitialSyncActivity initialSyncActivity);

    void inject(InventoryListActivity inventoryActivity);

    void inject(InventoryListFragment inventoryFragment);

    void inject(ItemDetailsFragment itemDetailsFragment);

    void inject(EditLoadoutActivity loadoutActivity);

    void inject(LoadoutsListFragment loadoutsListFragment);

    void inject(LoadoutDetailsFragment loadoutDetailsFragment);

    void inject(NewLoadoutSlotSelectionFragment newLoadoutSlotSelectionFragment);
}
