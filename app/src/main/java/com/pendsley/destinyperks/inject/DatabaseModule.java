package com.pendsley.destinyperks.inject;

import android.database.sqlite.SQLiteDatabase;

import com.pendsley.destinyperks.authentication.persistence.DestinyPerksDbHelper;
import com.pendsley.destinyperks.persistence.dao.ManifestHelper;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * TODO
 */
@Module
public class DatabaseModule {

    public static final String READABLE_DATABASE = "readableDatabase";
    public static final String WRITABLE_DATABASE = "writableDatabase";
    public static final String MANIFEST_DATABASE = "manifestDatabase";

    private final DestinyPerksDbHelper destingPerksDbeHelper;
    private final ManifestHelper manifestHelper;

    public DatabaseModule(DestinyPerksDbHelper destingPerksDbeHelper,
                          ManifestHelper manifestHelper) {
        this.destingPerksDbeHelper = destingPerksDbeHelper;
        this.manifestHelper = manifestHelper;
    }

    @Provides
    @Named(READABLE_DATABASE)
    SQLiteDatabase provideReadableDatabase() {
        return destingPerksDbeHelper.getReadableDatabase();
    }

    @Provides
    @Named(WRITABLE_DATABASE)
    SQLiteDatabase provideWritableDatabase() {
        return destingPerksDbeHelper.getWritableDatabase();
    }

    @Provides
    @Named(MANIFEST_DATABASE)
    SQLiteDatabase provideManifestDatabase() {
        return manifestHelper.getReadableDatabase();
    }
}

// TODO
//SELECT * FROM DestinySandboxPerkDefinition WHERE id + 4294967296 = 2151130854 OR id = 2151130854
