package com.pendsley.destinyperks.inject;

import com.pendsley.destinyperks.network.BungieApiHeaderInterceptor;
import com.pendsley.destinyperks.network.BungieDestinyService;
import com.pendsley.destinyperks.network.BungieUserService;
import com.pendsley.destinyperks.authentication.BungieAuthenticationService;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * TODO
 */
@Module
public class NetworkingModule {

    private static final String APP_RETROFIT = "appRetrofit";
    private static final String USER_RETROFIT = "userRetrofit";
    private static final String DESTINY_RETROFIT = "destinyRetrofit";

    @Provides
    @Singleton
    OkHttpClient provideHttpClient(BungieApiHeaderInterceptor bungieApiHeaderInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(bungieApiHeaderInterceptor)
                .build();
    }

    @Provides
    @Singleton
    GsonConverterFactory provideGsonConverterFactory() {
        return GsonConverterFactory.create();
    }

    @Provides
    @Singleton
    @Named(APP_RETROFIT)
    Retrofit provideAppRetrofit(OkHttpClient okHttpClient,
                                GsonConverterFactory gsonConverterFactory) {

        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https://www.bungie.net/Platform/App/")
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    @Provides
    @Singleton
    @Named(USER_RETROFIT)
    Retrofit provideUserServiceRetrofit(OkHttpClient okHttpClient,
                                        GsonConverterFactory gsonConverterFactory) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https:/www.bungie.net/Platform/User/")
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    @Provides
    @Singleton
    @Named(DESTINY_RETROFIT)
    Retrofit provideDestinyServiceRetrofit(OkHttpClient okHttpClient,
                                           GsonConverterFactory gsonConverterFactory) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https:/www.bungie.net/d1/Platform/Destiny/")
                .addConverterFactory(gsonConverterFactory)
                .build();
    }



    @Provides
    @Singleton
    BungieAuthenticationService provideBungieAuthenticationService(@Named(APP_RETROFIT) Retrofit retrofit) {

        return retrofit.create(BungieAuthenticationService.class);
    }

    @Provides
    @Singleton
    BungieUserService provideBungieUserService(@Named(USER_RETROFIT) Retrofit retrofit) {
        return retrofit.create(BungieUserService.class);
    }

    @Provides
    @Singleton
    BungieDestinyService provideBungieDestinyService(@Named(DESTINY_RETROFIT) Retrofit retrofit) {
        return retrofit.create(BungieDestinyService.class);
    }

}
