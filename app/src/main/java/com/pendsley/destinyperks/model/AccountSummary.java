package com.pendsley.destinyperks.model;

import java.util.List;

/**
 * TODO
 */
public class AccountSummary {

    private List<CharacterProfile> characters;
    private Inventory inventory;

    public List<CharacterProfile> getCharacters() {
        return characters;
    }

    public void setCharacters(List<CharacterProfile> characters) {
        this.characters = characters;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }
}
