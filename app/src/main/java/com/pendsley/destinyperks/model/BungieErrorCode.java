package com.pendsley.destinyperks.model;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * TODO
 */
public enum BungieErrorCode {

    SUCCESS(1, ""),
    ITEM_NOT_FOUND(1623, "Could not find item"),
    CANT_EQUIP_IN_COMBAT(1634, "Can't equip in combat"),
    EXOTIC_ALREADY_EQUIPPED(1641, "Exotic already equipped"),
    NO_ROOM_TO_TRANSFER(1642, "No free slots"),
    OTHER(-1, "Something unexpected happened");

    static {
        ImmutableMap.Builder<Integer, BungieErrorCode> errorCodesMap = ImmutableMap.builder();
        for (BungieErrorCode errorCode : values()) {
            errorCodesMap.put(errorCode.errorCode, errorCode);
        }
        ERROR_CODES_MAP = errorCodesMap.build();
    }

    private static final Map<Integer, BungieErrorCode> ERROR_CODES_MAP;

    private final int errorCode;
    private final String errorMessage;

    BungieErrorCode(int errorCode,
                    String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public static BungieErrorCode forCode(int errorCode) {
        BungieErrorCode bungieErrorCode = ERROR_CODES_MAP.get(errorCode);
        if (bungieErrorCode == null) {
            return OTHER;
        }
        return bungieErrorCode;
    }
}
