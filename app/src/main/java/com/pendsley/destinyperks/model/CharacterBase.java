package com.pendsley.destinyperks.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phillip on 8/27/2017.
 */

public class CharacterBase {

    private String characterId;
    private int classType;

    @SerializedName("powerLevel")
    private String lightLevel;

    public String getCharacterId() {
        return characterId;
    }

    public void setCharacterId(String characterId) {
        this.characterId = characterId;
    }

    public int getClassType() {
        return classType;
    }

    public void setClassType(int classType) {
        this.classType = classType;
    }

    public String getLightLevel() {
        return lightLevel;
    }

    public void setLightLevel(String lightLevel) {
        this.lightLevel = lightLevel;
    }
}
