package com.pendsley.destinyperks.model;

/**
 * Created by Phillip on 8/27/2017.
 */

public class CharacterClassDefinition {

    private int classType;
    private String className;

    public int getClassType() {
        return classType;
    }

    public void setClassType(int classType) {
        this.classType = classType;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
