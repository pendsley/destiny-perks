package com.pendsley.destinyperks.model;

/**
 * Created by Phillip on 8/27/2017.
 */

public class CharacterProfile {

    private String emblemPath;
    private String backgroundPath;
    private CharacterBase characterBase;

    public String getEmblemPath() {
        return emblemPath;
    }

    public void setEmblemPath(String emblemPath) {
        this.emblemPath = emblemPath;
    }

    public String getBackgroundPath() {
        return backgroundPath;
    }

    public void setBackgroundPath(String backgroundPath) {
        this.backgroundPath = backgroundPath;
    }

    public CharacterBase getCharacterBase() {
        return characterBase;
    }

    public void setCharacterBase(CharacterBase characterBase) {
        this.characterBase = characterBase;
    }
}
