package com.pendsley.destinyperks.model;

/**
 * Data for displaying the filter options.
 *
 * @author Phil Endsley
 */
public class InventoryFilterOption {

    private String description;
    private String emblemBackgroundPath;
    private int characterIndex;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmblemBackgroundPath() {
        return emblemBackgroundPath;
    }

    public void setEmblemBackgroundPath(String emblemBackgroundPath) {
        this.emblemBackgroundPath = emblemBackgroundPath;
    }

    public int getCharacterIndex() {
        return characterIndex;
    }

    public void setCharacterIndex(int characterIndex) {
        this.characterIndex = characterIndex;
    }
}
