package com.pendsley.destinyperks.model;

import android.support.annotation.Nullable;

/**
 * The different item slots.
 *
 * @author Phil Endsley
 */
public enum ItemSlot {

    PRIMARY("Primary"),
    SPECIAL("Special"),
    HEAVY("Heavy"),
    HELMET("Helmet"),
    GAUNTLETS("Gauntlets"),
    CHEST("Chest"),
    LEGS("Legs"),
    CLASS("Class Item"),
    ARTIFACT("Artifact"),
    GHOST("Ghost");

    private final String description;

    ItemSlot(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Nullable
    public static ItemSlot fromDescription(String description) {
        for (ItemSlot itemSlot : values()) {
            if (itemSlot.getDescription().equalsIgnoreCase(description)) {
                return itemSlot;
            }
        }

        return null;
    }
}
