package com.pendsley.destinyperks.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import java.util.Objects;

/**
 * Details for an item stored locally.
 *
 * @author Phil Endsley
 */
public class LocalInventoryItem implements Parcelable {

    private String instanceId;
    private String hashId;
    private String name;
    private Integer light;
    private String itemTypeName;
    private String itemType;
    private Integer characterIndex;
    private ItemClassType itemClassType;
    private Boolean equipped;
    private List<TalentNodeStep> talentNodeSteps;
    private List<Stat> stats;

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getHashId() {
        return hashId;
    }

    public void setHashId(String hashId) {
        this.hashId = hashId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLight() {
        return light;
    }

    public void setLight(Integer light) {
        this.light = light;
    }

    public String getItemTypeName() {
        return itemTypeName;
    }

    public void setItemTypeName(String itemTypeName) {
        this.itemTypeName = itemTypeName;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public Integer getCharacterIndex() {
        return characterIndex;
    }

    public void setCharacterIndex(Integer characterIndex) {
        this.characterIndex = characterIndex;
    }

    public ItemClassType getItemClassType() {
        return itemClassType;
    }

    public void setItemClassType(ItemClassType itemClassType) {
        this.itemClassType = itemClassType;
    }

    public Boolean isEquipped() {
        return equipped;
    }

    public void setEquipped(Boolean equipped) {
        this.equipped = equipped;
    }

    public List<TalentNodeStep> getTalentNodeSteps() {
        return talentNodeSteps;
    }

    public void setTalentNodeSteps(List<TalentNodeStep> talentNodeSteps) {
        this.talentNodeSteps = talentNodeSteps;
    }

    public List<Stat> getStats() {
        return stats;
    }

    public void setStats(List<Stat> stats) {
        this.stats = stats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LocalInventoryItem that = (LocalInventoryItem) o;

        return Objects.equals(instanceId, that.instanceId) &&
                Objects.equals(hashId, that.hashId) &&
                Objects.equals(name, that.name) &&
                Objects.equals(itemTypeName, that.itemTypeName) &&
                Objects.equals(itemType, that.itemType);
    }

    @Override
    public int hashCode() {
        int result = instanceId != null ? instanceId.hashCode() : 0;
        result = 31 * result + (hashId != null ? hashId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (itemTypeName != null ? itemTypeName.hashCode() : 0);
        result = 31 * result + (itemType != null ? itemType.hashCode() : 0);
        return result;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.instanceId);
        dest.writeString(this.hashId);
        dest.writeString(this.name);
        dest.writeValue(this.light);
        dest.writeString(this.itemTypeName);
        dest.writeString(this.itemType);
        dest.writeValue(this.characterIndex);
        dest.writeInt(this.itemClassType == null ? -1 : this.itemClassType.ordinal());
        dest.writeValue(this.equipped);
        dest.writeTypedList(this.talentNodeSteps);
        dest.writeTypedList(this.stats);
    }

    public LocalInventoryItem() {
    }

    protected LocalInventoryItem(Parcel in) {
        this.instanceId = in.readString();
        this.hashId = in.readString();
        this.name = in.readString();
        this.light = (Integer) in.readValue(Integer.class.getClassLoader());
        this.itemTypeName = in.readString();
        this.itemType = in.readString();
        this.characterIndex = (Integer) in.readValue(Integer.class.getClassLoader());
        int tmpItemClassType = in.readInt();
        this.itemClassType = tmpItemClassType == -1 ? null : ItemClassType.values()[tmpItemClassType];
        this.equipped = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.talentNodeSteps = in.createTypedArrayList(TalentNodeStep.CREATOR);
        this.stats = in.createTypedArrayList(Stat.CREATOR);
    }

    public static final Parcelable.Creator<LocalInventoryItem> CREATOR = new Parcelable.Creator<LocalInventoryItem>() {
        @Override
        public LocalInventoryItem createFromParcel(Parcel source) {
            return new LocalInventoryItem(source);
        }

        @Override
        public LocalInventoryItem[] newArray(int size) {
            return new LocalInventoryItem[size];
        }
    };
}
