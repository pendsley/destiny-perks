package com.pendsley.destinyperks.model;

/**
 * Stats data from the manifest.
 *
 * @author Phil Endsley
 */
public class ManifestStat {

    private String statName;

    public String getStatName() {
        return statName;
    }

    public void setStatName(String statName) {
        this.statName = statName;
    }

}
