package com.pendsley.destinyperks.model;

/**
 * The different stats for items.
 *
 * @author Phil Endsley
 */
@SuppressWarnings("unused") // Most of these are used in the forName method
public enum StatName {

    RATE_OF_FIRE("Rate of Fire", 0, false),
    CHARGE_RATE("Charge Rate", 1, false),
    BLAST_RADIUS("Blast Radius", 2, false),
    VELOCITY("Velocity", 3, false),
    SPEED("Speed", 4, false),
    IMPACT("Impact", 5, false),
    RANGE("Range", 6, false),
    STABILITY("Stability", 7, false),
    RELOAD("Reload", 8, false),
    EFFICIENCY("Efficiency", 9, false),
    DEFENSE("Defense", 10, false),
    ENERGY("Energy", 11, true),
    MAGAZINE("Magazine", 12, true),
    INTELLECT("Intellect", 13, true),
    DISCIPLINE("Discipline", 14, true),
    STRENGTH("Strength", 15, true),
    UNKNOWN("Unknown", 16, true);
    
    private final String description;
    private final int sortOrder;
    private final boolean textDisplay;
    
    StatName(String description,
             int sortOrder,
             boolean textDisplay) {
        this.description = description;
        this.sortOrder = sortOrder;
        this.textDisplay = textDisplay;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public String getDescription() {
        return description;
    }

    public boolean isTextDisplay() {
        return textDisplay;
    }

    public static StatName forName(String name) {
        for (StatName statName : values()) {
            if (name.equals(statName.getDescription())) {
                return statName;
            }
        }
        return UNKNOWN;
    }
}
