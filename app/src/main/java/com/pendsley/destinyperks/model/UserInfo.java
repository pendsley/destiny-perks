package com.pendsley.destinyperks.model;

/**
 * TODO
 */
public class UserInfo {

    private Double membershipType;
    private String membershipId;
    private String displayName;

    public Double getMembershipType() {
        return membershipType;
    }

    public void setMembershipType(Double membershipType) {
        this.membershipType = membershipType;
    }

    public String getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
