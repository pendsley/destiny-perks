package com.pendsley.destinyperks.model;

/**
 * Vault representation of a {@link Character}.
 *
 * @author Phil Endsley
 */
public class VaultDestination extends Character {

    @Override
    public Integer getCharacterIndex() {
        return -1;
    }

}
