package com.pendsley.destinyperks.network;

import com.pendsley.destinyperks.inject.AuthenticationModule;

import java.io.IOException;
import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * TODO
 */
public class BungieApiHeaderInterceptor implements Interceptor {

    private final String accessToken;
    private final Long accessTokenExpiration;

    public BungieApiHeaderInterceptor(@Named(AuthenticationModule.ACCESS_TOKEN) String accessToken,
                                      @Named(AuthenticationModule.ACCESS_TOKEN_EXPIRATION_TIME) Long accessTokenExpiration) {
        this.accessToken = accessToken;
        this.accessTokenExpiration = accessTokenExpiration;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request originalRequest = chain.request();

        Request.Builder requestBuilder = originalRequest.newBuilder()
                .header("X-API-Key", "7191206dfcf944c4900885e7172c0b1d")
                .method(originalRequest.method(), originalRequest.body());

        // Only if we have it
        if (accessToken != null && !accessToken.isEmpty() && new Date().getTime() < accessTokenExpiration) {
            requestBuilder.header("Authorization", "Bearer " + accessToken);
        }

        return chain.proceed(requestBuilder.build());
    }
}
