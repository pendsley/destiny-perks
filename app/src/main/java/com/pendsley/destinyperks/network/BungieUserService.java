package com.pendsley.destinyperks.network;

import com.pendsley.destinyperks.model.BungieAccountData;
import com.pendsley.destinyperks.model.BungieResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * TODO
 */
public interface BungieUserService {

    @GET("GetCurrentBungieAccount/")
    Call<BungieResponse<BungieAccountData>> getCurrentBungieAccount();

    @GET("GetMembershipsForCurrentUser/")
    Call<BungieResponse<BungieAccountData>> getMembershipsForCurrentUser();


}
