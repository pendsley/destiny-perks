package com.pendsley.destinyperks.operation;

import com.google.common.base.Optional;
import com.google.firebase.crash.FirebaseCrash;
import com.pendsley.destinyperks.inject.AccountModule;
import com.pendsley.destinyperks.model.BungieErrorCode;
import com.pendsley.destinyperks.model.BungieResponse;
import com.pendsley.destinyperks.model.EquipItemArguments;
import com.pendsley.destinyperks.model.EquipOperationResult;
import com.pendsley.destinyperks.model.LocalInventoryItem;
import com.pendsley.destinyperks.network.BungieDestinyService;
import com.pendsley.destinyperks.persistence.dao.LocalInventoryDao;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.annotations.NonNull;
import retrofit2.Response;

/**
 * Operation for equipping items.
 *
 * @author Phil Endsley
 */
public class EquipItemOperation {

    private final BungieDestinyService destinyService;
    private final InventoryLookupOperation inventoryLookupOperation;
    private final LocalInventoryDao localInventoryDao;
    private final int membershipType;

    @Inject
    EquipItemOperation(BungieDestinyService destinyService,
                       InventoryLookupOperation inventoryLookupOperation,
                       LocalInventoryDao localInventoryDao,
                       @Named(AccountModule.MEMBERSHIP_TYPE) Double membershipType) {
        this.destinyService = destinyService;
        this.inventoryLookupOperation = inventoryLookupOperation;
        this.localInventoryDao = localInventoryDao;
        this.membershipType = membershipType.intValue();
    }

    public Flowable<EquipOperationResult> equipItem(final LocalInventoryItem item,
                                                    final String characterId) {
        return Flowable.create(new FlowableOnSubscribe<EquipOperationResult>() {
            @Override
            public void subscribe(@NonNull FlowableEmitter<EquipOperationResult> emitter) throws Exception {
                final EquipItemArguments equipItemArguments = new EquipItemArguments(membershipType, item.getInstanceId(),
                        characterId);

                final String itemType = item.getItemType();
                final int characterIndex = item.getCharacterIndex();

                try {

                    Response<BungieResponse<Object>> responseWrapper = destinyService.equipItem(equipItemArguments).execute();

                    BungieResponse<Object> response = responseWrapper.body();

                    BungieErrorCode errorCode = BungieErrorCode.forCode(response.getErrorCode());

                    if (BungieErrorCode.SUCCESS == errorCode) {
                        Optional<LocalInventoryItem> previousItemOptional =
                                inventoryLookupOperation.getCurrentlyEquippedItem(itemType, characterIndex);

                        LocalInventoryItem previousItem = null;
                        String previousInstanceId = null;

                        if (previousItemOptional.isPresent()) {

                            previousItem = previousItemOptional.get();
                            previousInstanceId = previousItem.getInstanceId();
                            previousItem.setEquipped(false);

                        } else {
                            FirebaseCrash.report(new Exception(String.format("Could not find previously equipped item on character [%s]", characterId)));
                        }

                        localInventoryDao.updateWeaponEquippedStatus(item.getInstanceId(),
                                previousInstanceId);

                        item.setEquipped(true);

                        emitter.onNext(new EquipOperationResult(previousItem, item));
                        emitter.onComplete();

                    } else {
                        throw new RuntimeException(errorCode.getErrorMessage());
                    }

                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }, BackpressureStrategy.BUFFER);
    }

}
