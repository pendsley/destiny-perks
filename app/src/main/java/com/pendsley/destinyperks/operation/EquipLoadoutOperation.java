package com.pendsley.destinyperks.operation;

import android.support.annotation.Nullable;

import com.google.common.base.Strings;
import com.pendsley.destinyperks.inject.AccountModule;
import com.pendsley.destinyperks.model.Character;
import com.pendsley.destinyperks.model.EquipOperationResult;
import com.pendsley.destinyperks.model.Loadout;
import com.pendsley.destinyperks.model.LocalInventoryItem;
import com.pendsley.destinyperks.persistence.dao.LocalInventoryDao;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.Scheduler;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

/**
 * Transfers and equips all items in a loadout to a character.
 *
 * @author Phil Endsley
 */
public class EquipLoadoutOperation {

    private final TransferItemOperation transferItemOperation;
    private final EquipItemOperation equipItemOperation;
    private final InventoryLookupOperation inventoryLookupOperation;
    private final LocalInventoryDao localInventoryDao;
    private final List<Character> characters;

    @Inject
    EquipLoadoutOperation(TransferItemOperation transferItemOperation,
                          EquipItemOperation equipItemOperation,
                          InventoryLookupOperation inventoryLookupOperation,
                          LocalInventoryDao localInventoryDao,
                          @Named(AccountModule.CHARACTERS) List<Character> characters) {
        this.transferItemOperation = transferItemOperation;
        this.equipItemOperation = equipItemOperation;
        this.inventoryLookupOperation = inventoryLookupOperation;
        this.localInventoryDao = localInventoryDao;
        this.characters = characters;
    }

    public List<Flowable<EquipOperationResult>> equipLoadout(Loadout loadout,
                                                             Scheduler subscribeOnScheduler) {

        String primaryInstanceId = loadout.getPrimaryItemInstanceId();
        final String specialInstanceId = loadout.getSpecialItemInstanceId();
        final String heavyInstanceId = loadout.getHeavyItemInstanceId();

        final String characterId = loadout.getCharacterId();

        List<Flowable<EquipOperationResult>> flowables = new ArrayList<>();
        flowables.add(transferAndEquipItem(primaryInstanceId, characterId, subscribeOnScheduler));
        flowables.add(transferAndEquipItem(specialInstanceId, characterId, subscribeOnScheduler));
        flowables.add(transferAndEquipItem(heavyInstanceId, characterId, subscribeOnScheduler));
        return flowables;
    }

    private Flowable<EquipOperationResult> transferAndEquipItem(final String instanceId,
                                                                final String characterId,
                                                                final Scheduler subscribeOnScheduler) {


        if (Strings.isNullOrEmpty(instanceId)) {
            return Flowable.empty();
        }

        return Flowable.create(new FlowableOnSubscribe<LocalInventoryItem>() {
            @Override
            public void subscribe(@NonNull FlowableEmitter<LocalInventoryItem> emitter) throws Exception {

                LocalInventoryItem item = localInventoryDao.getLocalItemByInstanceId(instanceId);
                item.setTalentNodeSteps(inventoryLookupOperation.getTalentNodes(instanceId));
                item.setStats(inventoryLookupOperation.getStats(instanceId));
                emitter.onNext(item);
                emitter.onComplete();
            }
        }, BackpressureStrategy.BUFFER)
                .concatMap(new Function<LocalInventoryItem, Flowable<LocalInventoryItem>>() {

                    @Override
                    public Flowable<LocalInventoryItem> apply(@NonNull LocalInventoryItem item) throws Exception {

                        int characterIndex = item.getCharacterIndex();

                        final Character destinationCharacter = getChacter(characterId);
                        String currentCharacterId = null;
                        if (characterIndex >= 0) {
                            currentCharacterId = characters.get(item.getCharacterIndex()).getCharacterId();
                        }

                        boolean shouldTransfer = !characterId.equals(currentCharacterId);

                        if (shouldTransfer) {
                            return transferItemOperation.transferItem(destinationCharacter, item);

                        } else {
                            return Flowable.just(item);
                        }
                    }
                }).concatMap(new Function<LocalInventoryItem, Flowable<EquipOperationResult>>() {
                    @Override
                    public Flowable<EquipOperationResult> apply(@NonNull LocalInventoryItem transferredItem) throws Exception {
                        return equipItemOperation.equipItem(transferredItem, characterId);
                    }
                }).subscribeOn(subscribeOnScheduler);
    }

    @Nullable
    private Character getChacter(String characterId) {
        for (Character character : characters) {
            if (characterId.equals(character.getCharacterId())) {
                return character;
            }
        }

        return null;
    }
}
