package com.pendsley.destinyperks.operation;

import android.support.annotation.Nullable;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.pendsley.destinyperks.model.ItemClassType;
import com.pendsley.destinyperks.model.ItemType;
import com.pendsley.destinyperks.model.LocalInventoryItem;
import com.pendsley.destinyperks.model.Stat;
import com.pendsley.destinyperks.model.StatName;
import com.pendsley.destinyperks.model.TalentNodeStep;
import com.pendsley.destinyperks.persistence.dao.ItemStatsDao;
import com.pendsley.destinyperks.persistence.dao.LocalInventoryDao;
import com.pendsley.destinyperks.persistence.dao.TalentNodeDao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

/**
 * Retrieves inventory items based on various search parameters.
 *
 * @author Phil Endsley
 */
public class InventoryLookupOperation {

    private final LocalInventoryDao localInventoryDao;
    private final TalentNodeDao talentNodeDao;
    private final ItemStatsDao itemStatsDao;

    @Inject
    InventoryLookupOperation(LocalInventoryDao localInventoryDao,
                             TalentNodeDao talentNodeDao,
                             ItemStatsDao itemStatsDao) {
        this.localInventoryDao = localInventoryDao;
        this.talentNodeDao = talentNodeDao;
        this.itemStatsDao = itemStatsDao;
    }

    public Flowable<List<LocalInventoryItem>> getLookupByClassTypeFlowable(ItemClassType itemClassType) {
        return localInventoryDao.getLocalItemsByClassTypeFlowable(itemClassType)
                .map(new Function<List<LocalInventoryItem>, List<LocalInventoryItem>>() {
                    @Override
                    public List<LocalInventoryItem> apply(@NonNull List<LocalInventoryItem> inventoryItems) throws Exception {
                        return new ArrayList<>(Lists.transform(inventoryItems, new com.google.common.base.Function<LocalInventoryItem, LocalInventoryItem>() {
                            @Override
                            public LocalInventoryItem apply(LocalInventoryItem item) {

                                item.setTalentNodeSteps(getTalentNodes(item.getInstanceId()));
                                return item;
                            }
                        }));
                    }
                }).map(new Function<List<LocalInventoryItem>, List<LocalInventoryItem>>() {
                    @Override
                    public List<LocalInventoryItem> apply(@NonNull List<LocalInventoryItem> inventoryItems) throws Exception {
                        return new ArrayList<>(Lists.transform(inventoryItems, new com.google.common.base.Function<LocalInventoryItem, LocalInventoryItem>() {
                            @Override
                            public LocalInventoryItem apply(LocalInventoryItem item) {

                                item.setStats(getStats(item.getInstanceId()));
                                return item;
                            }
                        }));
                    }
                });
    }

    public Flowable<List<LocalInventoryItem>> getLookupByCategoryFlowable(String itemCategory) {
        return localInventoryDao.getLocalItemsByCategoryFlowable(itemCategory)
                .map(new Function<List<LocalInventoryItem>, List<LocalInventoryItem>>() {
                    @Override
                    public List<LocalInventoryItem> apply(@NonNull List<LocalInventoryItem> inventoryItems) throws Exception {
                        return new ArrayList<>(Lists.transform(inventoryItems, new com.google.common.base.Function<LocalInventoryItem, LocalInventoryItem>() {
                            @Override
                            public LocalInventoryItem apply(LocalInventoryItem item) {

                                item.setTalentNodeSteps(getTalentNodes(item.getInstanceId()));
                                return item;
                            }
                        }));
                    }
                }).map(new Function<List<LocalInventoryItem>, List<LocalInventoryItem>>() {
                    @Override
                    public List<LocalInventoryItem> apply(@NonNull List<LocalInventoryItem> inventoryItems) throws Exception {
                        return new ArrayList<>(Lists.transform(inventoryItems, new com.google.common.base.Function<LocalInventoryItem, LocalInventoryItem>() {
                            @Override
                            public LocalInventoryItem apply(LocalInventoryItem item) {

                                item.setStats(getStats(item.getInstanceId()));
                                return item;
                            }
                        }));
                    }
                });
    }

    Optional<LocalInventoryItem> getCurrentlyEquippedItem(String itemCategory,
                                                          int characterIndex) {
        Optional<LocalInventoryItem> itemOptional = localInventoryDao.getCurrentlyEquippedItem(itemCategory, characterIndex);
        if (itemOptional.isPresent()) {
            String itemInstanceId = itemOptional.get().getInstanceId();
            itemOptional.get().setTalentNodeSteps(getTalentNodes(itemInstanceId));
            itemOptional.get().setStats(getStats(itemInstanceId));
        }

        return itemOptional;
    }

    @Nullable
    LocalInventoryItem getItemByInstanceId(String itemInstanceId) {
        LocalInventoryItem item = localInventoryDao.getLocalItemByInstanceId(itemInstanceId);
        if (item == null) {
            return null;
        }
        item.setTalentNodeSteps(getTalentNodes(itemInstanceId));
        item.setStats(getStats(itemInstanceId));
        return item;
    }

    List<TalentNodeStep> getTalentNodes(String itemInstanceId) {
        List<TalentNodeStep> talentNodeSteps = talentNodeDao.getTalentNodes(itemInstanceId);
        Collections.sort(talentNodeSteps, new Comparator<TalentNodeStep>() {
            @Override
            public int compare(TalentNodeStep talentNodeStep1, TalentNodeStep talentNodeStep2) {
                int comparison = talentNodeStep1.getColumn().compareTo(talentNodeStep2.getColumn());
                if (comparison == 0) {
                    return talentNodeStep1.getRow().compareTo(talentNodeStep2.getRow());
                } else {
                    return comparison;
                }
            }
        });

        return talentNodeSteps;
    }

    List<Stat> getStats(String itemInstanceId) {
        List<Stat> stats = itemStatsDao.getStatsForInstanceId(itemInstanceId);
        Collections.sort(stats, new Comparator<Stat>() {
            @Override
            public int compare(Stat stat1, Stat stat2) {
                Integer value1 = StatName.forName(stat1.getName()).getSortOrder();
                Integer value2 = StatName.forName(stat2.getName()).getSortOrder();

                return value1.compareTo(value2);
            }
        });

        return stats;
    }


}
