package com.pendsley.destinyperks.operation;

import com.pendsley.destinyperks.model.Loadout;
import com.pendsley.destinyperks.persistence.dao.LoadoutDao;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;

/**
 * Retrieves all loadouts.
 *
 * @author Phil Endsley
 */
public class LoadoutsListOperation {

    private final LoadoutDao loadoutDao;

    @Inject
    LoadoutsListOperation(LoadoutDao loadoutDao) {
        this.loadoutDao = loadoutDao;
    }

    public Flowable<List<Loadout>> getLoadoutsForCharacterFlowable(String characterId) {
        return loadoutDao.getLoadoutsFlowable(characterId);
    }
}
