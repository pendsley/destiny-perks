package com.pendsley.destinyperks.operation;

import com.pendsley.destinyperks.inject.AccountModule;
import com.pendsley.destinyperks.model.BungieErrorCode;
import com.pendsley.destinyperks.model.BungieResponse;
import com.pendsley.destinyperks.model.Character;
import com.pendsley.destinyperks.model.LocalInventoryItem;
import com.pendsley.destinyperks.model.TransferItemArguments;
import com.pendsley.destinyperks.model.VaultDestination;
import com.pendsley.destinyperks.network.BungieDestinyService;
import com.pendsley.destinyperks.persistence.dao.LocalInventoryDao;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.annotations.NonNull;
import retrofit2.Response;

/**
 * Operation for transferring items.
 *
 * @author Phil Endsley
 */
public class TransferItemOperation {

    private final Double membershipType;

    private final BungieDestinyService destinyService;
    private final LocalInventoryDao localInventoryDao;
    private final List<Character> characters;

    @Inject
    TransferItemOperation(BungieDestinyService destinyService,
                          LocalInventoryDao localInventoryDao,
                          @Named(AccountModule.MEMBERSHIP_TYPE) Double membershipType,
                          @Named(AccountModule.CHARACTERS) List<Character> characters) {
        this.destinyService = destinyService;
        this.localInventoryDao = localInventoryDao;
        this.membershipType = membershipType;
        this.characters = characters;
    }

    public Flowable<LocalInventoryItem> transferItem(final Character destinationCharacter,
                                                     final LocalInventoryItem item) {


        int currentCharacterIndex = item.getCharacterIndex();
        final Character currentCharacterLocation = currentCharacterIndex >= 0 ? characters.get(currentCharacterIndex) : null;
        boolean moveToVault = currentCharacterLocation != null;

        Flowable<LocalInventoryItem> transferFlowable;

        if (moveToVault) {

            if (destinationCharacter instanceof VaultDestination) {
                transferFlowable = Flowable.create(new FlowableOnSubscribe<LocalInventoryItem>() {
                    @Override
                    public void subscribe(@NonNull FlowableEmitter<LocalInventoryItem> emitter) throws Exception {
                        emitter.onNext(performTransfer(item,
                                destinationCharacter,
                                currentCharacterLocation.getCharacterId(),
                                true));
                        emitter.onComplete();
                    }}, BackpressureStrategy.BUFFER);
            } else {

                transferFlowable = Flowable.create(new FlowableOnSubscribe<LocalInventoryItem>() {
                    @Override
                    public void subscribe(@NonNull FlowableEmitter<LocalInventoryItem> emitter) throws Exception {
                        LocalInventoryItem transferredItem = performTransfer(item,
                                destinationCharacter,
                                currentCharacterLocation.getCharacterId(),
                                true);
                        emitter.onNext(performTransfer(transferredItem,
                                currentCharacterLocation,
                                currentCharacterLocation.getCharacterId(),
                                false));
                        emitter.onComplete();
                    }}, BackpressureStrategy.BUFFER);
            }

        } else {
            transferFlowable = Flowable.create(new FlowableOnSubscribe<LocalInventoryItem>() {
                @Override
                public void subscribe(@NonNull FlowableEmitter<LocalInventoryItem> emitter) throws Exception {

                    emitter.onNext(performTransfer(item,
                            destinationCharacter,
                            destinationCharacter.getCharacterId(),
                            false));
                    emitter.onComplete();
                }}, BackpressureStrategy.BUFFER);
        }

        return transferFlowable;
    }

    private LocalInventoryItem performTransfer(final LocalInventoryItem item,
                                               final Character destinationCharacter,
                                               final String characterId,
                                               final boolean movingToVault) throws IOException {


        final TransferItemArguments transferItemArguments = new TransferItemArguments(membershipType.intValue(),
                item.getInstanceId(),
                characterId,
                new BigDecimal(item.getHashId()),
                movingToVault);

        Response<BungieResponse<Object>> transferResponse = destinyService.transferItem(transferItemArguments).execute();

        BungieErrorCode errorCode = BungieErrorCode.forCode(transferResponse.body().getErrorCode());


        if (BungieErrorCode.SUCCESS == errorCode) {

            localInventoryDao.updateWeaponCharacterIndex(item.getInstanceId(),
                    destinationCharacter.getCharacterIndex());

            item.setCharacterIndex(destinationCharacter.getCharacterIndex());

            return item;

        } else {
            String errorMessage = errorCode.getErrorMessage();
            // Bungie services return an error code of 1623 - ItemNotFound if the vault is full when
            // trying to transfer to it.
            if (movingToVault && BungieErrorCode.ITEM_NOT_FOUND == errorCode) {
                errorMessage = "Vault is full";
            }
            throw new RuntimeException(errorMessage);
        }
    }


}

