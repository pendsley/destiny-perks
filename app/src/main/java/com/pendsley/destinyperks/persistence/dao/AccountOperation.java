package com.pendsley.destinyperks.persistence.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.common.collect.Lists;
import com.pendsley.destinyperks.authentication.persistence.CharacterContract;
import com.pendsley.destinyperks.inject.DatabaseModule;
import com.pendsley.destinyperks.model.Character;
import com.pendsley.destinyperks.model.ClassInformation;
import com.pendsley.destinyperks.model.UserInfo;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Database operations for user account and character information.
 *
 * @author Phil endsley
 */
public class AccountOperation {

    private final SQLiteDatabase sqLiteDatabase;

    @Inject
    public AccountOperation(@Named(DatabaseModule.WRITABLE_DATABASE) SQLiteDatabase sqLiteDatabase) {
        this.sqLiteDatabase = sqLiteDatabase;
    }

    public void saveUserInfo(UserInfo userInfo) {

        ContentValues contentValues = new ContentValues(3);
        contentValues.put(AccountContract.AccountEntry.COLUMN_NAME_DISPLAY_NAME,
                userInfo.getDisplayName());
        contentValues.put(AccountContract.AccountEntry.COLUMN_NAME_MEMBERSHIP_ID,
                userInfo.getMembershipId());
        contentValues.put(AccountContract.AccountEntry.COLUMN_NAME_MEMBERSHIP_TYPE,
                userInfo.getMembershipType());

        sqLiteDatabase.insert(AccountContract.AccountEntry.TABLE_NAME, null, contentValues);
    }

    public void deleteUserInfo() {
        sqLiteDatabase.delete(AccountContract.AccountEntry.TABLE_NAME, null, null);
    }

    public void saveCharacterInfo(String membershipId,
                                  List<Character> characters) {

        for (int i = 0; i < characters.size(); i++) {
            Character character = characters.get(i);
            ContentValues contentValues = new ContentValues(7);
            contentValues.put(CharacterContract.CharacterEntry.COLUMN_NAME_MEMBERSHIP_ID, membershipId);
            contentValues.put(CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_ID, character.getCharacterId());
            contentValues.put(CharacterContract.CharacterEntry.COLUMN_NAME_EMBLEM_PATH, character.getEmblemPath());
            contentValues.put(CharacterContract.CharacterEntry.COLUMN_NAME_BACKGROUND_PATH, character.getBackgroundPath());
            contentValues.put(CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_CLASS, character.getCharacterClass().getClassName());
            contentValues.put(CharacterContract.CharacterEntry.COLUMN_NAME_LIGHT_LEVEL, character.getLightLevel());
            contentValues.put(CharacterContract.CharacterEntry.COLUMN_CHARACTER_INDEX, i);

            sqLiteDatabase.insert(CharacterContract.CharacterEntry.TABLE_NAME, null, contentValues);
        }
    }

    public void deleteCharacterInfo() {
        sqLiteDatabase.delete(CharacterContract.CharacterEntry.TABLE_NAME, null, null);
    }

    public String getMembershipId() {

        try (Cursor cursor = sqLiteDatabase.query(AccountContract.AccountEntry.TABLE_NAME,
                new String[] { AccountContract.AccountEntry.COLUMN_NAME_MEMBERSHIP_ID },
                null,
                null,
                null,
                null,
                null)) {

            String membershipId = "";

            if (cursor.moveToNext()) {
                membershipId = cursor.getString(cursor.getColumnIndexOrThrow(AccountContract.AccountEntry.COLUMN_NAME_MEMBERSHIP_ID));
            }

            return membershipId;
        }
    }

    public Double getMembershipType(String memberShipId) {

        try (Cursor cursor = sqLiteDatabase.query(AccountContract.AccountEntry.TABLE_NAME,
                new String[] { AccountContract.AccountEntry.COLUMN_NAME_MEMBERSHIP_TYPE },
                AccountContract.AccountEntry.COLUMN_NAME_MEMBERSHIP_ID + " = ?",
                new String[] { memberShipId },
                null,
                null,
                null)) {

            Double membershipType = 0D;

            if (cursor.moveToNext()) {
                membershipType = cursor.getDouble(cursor.getColumnIndexOrThrow(AccountContract.AccountEntry.COLUMN_NAME_MEMBERSHIP_TYPE));
            }

            return membershipType;
        }
    }

    public List<String> getCharacterIds(String membershipId) {

        try (Cursor cursor = sqLiteDatabase.query(CharacterContract.CharacterEntry.TABLE_NAME,
                new String[] { CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_ID },
                CharacterContract.CharacterEntry.COLUMN_NAME_MEMBERSHIP_ID + " = ?",
                new String[] { membershipId },
                null,
                null,
                null)) {

            List<String> characterIds = new ArrayList<>();

            while (cursor.moveToNext()) {
                characterIds.add(cursor.getString(cursor.getColumnIndexOrThrow(CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_ID)));
            }

            return characterIds;
        }
    }

    public List<Character> getCharacters() {

        List<Character> characters = Lists.newArrayList();

        try (Cursor cursor = sqLiteDatabase.query(false,
                CharacterContract.CharacterEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null,
                null)) {

            while (cursor.moveToNext()) {
                Character character = new Character();
                character.setCharacterId(cursor.getString(cursor.getColumnIndexOrThrow(CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_ID)));
                character.setEmblemPath(cursor.getString(cursor.getColumnIndexOrThrow(CharacterContract.CharacterEntry.COLUMN_NAME_EMBLEM_PATH)));
                character.setBackgroundPath(cursor.getString(cursor.getColumnIndexOrThrow(CharacterContract.CharacterEntry.COLUMN_NAME_BACKGROUND_PATH)));
                character.setLightLevel(cursor.getInt(cursor.getColumnIndexOrThrow(CharacterContract.CharacterEntry.COLUMN_NAME_LIGHT_LEVEL)));
                ClassInformation classInformation = new ClassInformation();
                classInformation.setClassName(cursor.getString(cursor.getColumnIndexOrThrow(CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_CLASS)));
                character.setCharacterClass(classInformation);
                character.setCharacterIndex(cursor.getInt(cursor.getColumnIndexOrThrow(CharacterContract.CharacterEntry.COLUMN_CHARACTER_INDEX)));
                characters.add(character);
            }

        }

        return characters;
    }

    public List<String> getCharacterEmblemPaths() {

        List<String> emblemPaths = Lists.newArrayList();

        try (Cursor cursor = sqLiteDatabase.query(false,
                CharacterContract.CharacterEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null,
                null)) {

            while (cursor.moveToNext()) {
                emblemPaths.add(cursor.getString(cursor.getColumnIndexOrThrow(CharacterContract.CharacterEntry.COLUMN_NAME_EMBLEM_PATH)));
            }
        }

        return emblemPaths;
    }
}
