package com.pendsley.destinyperks.persistence.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.gson.Gson;
import com.pendsley.destinyperks.inject.DatabaseModule;
import com.pendsley.destinyperks.model.CharacterClassDefinition;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Phillip on 8/27/2017.
 */

public class CharacterOperation {

    private final SQLiteDatabase manifestDatabase;
    private final Gson gson;

    @Inject
    CharacterOperation(@Named(DatabaseModule.MANIFEST_DATABASE) SQLiteDatabase sqLiteDatabase,
                       Gson gson) {
        this.manifestDatabase = sqLiteDatabase;
        this.gson = gson;
    }

    public String getClassFromType(int classType) {

        try (Cursor cursor = manifestDatabase.query(ManifestTables.DESTINY_CLASS_DEFINITION,
                                                    new String[] {ManifestTables.COLUMN_NAME_JSON},
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null)) {
            while (cursor.moveToNext()) {
                String json = cursor.getString(cursor.getColumnIndexOrThrow(ManifestTables.COLUMN_NAME_JSON));
                CharacterClassDefinition characterClassDefinition = gson.fromJson(json, CharacterClassDefinition.class);
                if (characterClassDefinition.getClassType() == classType) {
                    return characterClassDefinition.getClassName();
                }
            }
        }

        return "";
    }
}
