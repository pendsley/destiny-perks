package com.pendsley.destinyperks.persistence.dao;

/**
 * TODO
 * @param <T>
 */
public interface InsertOperation<T> {

    void insert(T objectToInsert);

}
