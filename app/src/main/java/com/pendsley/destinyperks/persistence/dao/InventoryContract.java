package com.pendsley.destinyperks.persistence.dao;

import android.provider.BaseColumns;

/**
 * TODO
 */
public class InventoryContract {

    private InventoryContract() {
    }

    public static class InventoryEntry implements BaseColumns {

        public static final String TABLE_NAME = "inventory";
        public static final String COLUMN_NAME_ITEM_HASH = "item_hash";
        public static final String COLUMN_NAME_ITEM_INSTANCE_ID = "item_instance_id";
        public static final String COLUMN_NAME_ITEM_NAME = "name";
        public static final String COLUMN_NAME_LIGHT = "light";
        public static final String COLUMN_ITEM_TYPE_NAME = "item_type_name";
        public static final String COLUMN_ITEM_CATEGORY= "item_category";
        public static final String COLUMN_CHARACTER_INDEX = "character_index";
        public static final String COLUMN_ITEM_CLASS_TYPE = "class_type";
        public static final String COLUMN_EQUIPPED = "equipped";
    }
}
