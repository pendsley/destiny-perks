package com.pendsley.destinyperks.persistence.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.firebase.crash.FirebaseCrash;
import com.pendsley.destinyperks.inject.DatabaseModule;
import com.pendsley.destinyperks.model.Item;
import com.pendsley.destinyperks.model.ItemClassType;
import com.pendsley.destinyperks.model.ItemType;
import com.pendsley.destinyperks.model.LocalInventoryItem;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.annotations.NonNull;

/**
 * Database operations for local inventory items.
 *
 * @author Phil Endsley
 */
public class LocalInventoryDao {

    private static final String TAG = "LocalInventoryDao";

    private final SQLiteDatabase sqLiteDatabase;

    @Inject
    LocalInventoryDao(@Named(DatabaseModule.WRITABLE_DATABASE) SQLiteDatabase sqLiteDatabase) {
        this.sqLiteDatabase = sqLiteDatabase;
    }

    public Map<String, LocalInventoryItem> getAllLocalItemsMap() {

        Map<String, LocalInventoryItem> itemsMap = Maps.newConcurrentMap();

        try (Cursor cursor = sqLiteDatabase.query(false,
                InventoryContract.InventoryEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null,
                null)) {

            while (cursor.moveToNext()) {
                LocalInventoryItem item = createItem(cursor);
                itemsMap.put(item.getInstanceId(), item);
            }
        }

        return itemsMap;
    }

    public Flowable<List<LocalInventoryItem>> getLocalItemsByClassTypeFlowable(final ItemClassType itemClassType) {
        return Flowable.create(new FlowableOnSubscribe<List<LocalInventoryItem>>() {
            @Override
            public void subscribe(@NonNull FlowableEmitter<List<LocalInventoryItem>> emitter) throws Exception {
                emitter.onNext(getLocalItemsByClassType(itemClassType));
                emitter.onComplete();
            }
        }, BackpressureStrategy.BUFFER);
    }

    private List<LocalInventoryItem> getLocalItemsByClassType(ItemClassType itemClassType) {

        List<LocalInventoryItem> items = Lists.newArrayList();

        String where = null;
        String[] selectionArgs = null;

        if (ItemClassType.ALL != itemClassType) {
            where = InventoryContract.InventoryEntry.COLUMN_ITEM_CLASS_TYPE + "= ?";
            selectionArgs = new String[] {itemClassType.name()};
        }

        try (Cursor cursor = sqLiteDatabase.query(false,
                InventoryContract.InventoryEntry.TABLE_NAME,
                null,
                where,
                selectionArgs,
                null,
                null,
                null,
                null)) {

            while (cursor.moveToNext()) {
                items.add(createItem(cursor));
            }
        }

        return items;
    }

    public Flowable<List<LocalInventoryItem>> getLocalItemsByCategoryFlowable(final String itemCategory) {
        return Flowable.create(new FlowableOnSubscribe<List<LocalInventoryItem>>() {
            @Override
            public void subscribe(@NonNull FlowableEmitter<List<LocalInventoryItem>> emitter) throws Exception {
                emitter.onNext(getLocalItemsByTypeDescription(itemCategory));
                emitter.onComplete();
            }
        }, BackpressureStrategy.BUFFER);
    }

    private List<LocalInventoryItem> getLocalItemsByTypeDescription(String itemTypeDescription) {

        List<LocalInventoryItem> items = Lists.newArrayList();

        try (Cursor cursor = sqLiteDatabase.query(false,
                InventoryContract.InventoryEntry.TABLE_NAME,
                null,
                InventoryContract.InventoryEntry.COLUMN_ITEM_CATEGORY + "= ?",
                new String[]{itemTypeDescription},
                null,
                null,
                null,
                null)) {

            while (cursor.moveToNext()) {
                items.add(createItem(cursor));
            }
        }

        return items;
    }

    @Nullable
    public LocalInventoryItem getLocalItemByInstanceId(String instanceId) {
        try (Cursor cursor = sqLiteDatabase.query(InventoryContract.InventoryEntry.TABLE_NAME,
                null,
                InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_INSTANCE_ID + " = ?",
                new String[]{instanceId},
                null,
                null,
                null)) {

            if (cursor.moveToNext()) {
                return createItem(cursor);
            }
        }

        return null;
    }

    public void batchInsertItems(Set<Item> items) {

        sqLiteDatabase.beginTransaction();
        for (Item item : items) {

            ContentValues contentValues = new ContentValues();
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_INSTANCE_ID, item.getItemInstanceId());
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_HASH, item.getItemHash());
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_NAME, item.getItemName());
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_NAME_LIGHT, item.getLight());
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_ITEM_TYPE_NAME, item.getItemTypeName());
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_CHARACTER_INDEX, item.getCharacterIndex());
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_ITEM_CLASS_TYPE, item.getItemClass().name());
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_ITEM_CATEGORY, ItemType.descriptionForKeys(item.getItemCategoryHashes()));
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_EQUIPPED, item.getEquipped());

            sqLiteDatabase.insert(InventoryContract.InventoryEntry.TABLE_NAME, null, contentValues);
        }
        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
    }

    public void deleteItems(Set<String> itemIds) {
        String where = InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_INSTANCE_ID + " IN (";
        int count = 0;
        for (String itemId : itemIds) {
            if (count > 0) {
                where = where + ",";
            }
            where = where + itemId;
            count++;
        }
        where = where + ")";

        sqLiteDatabase.delete(InventoryContract.InventoryEntry.TABLE_NAME, where, null);
    }

    public void updateWeaponCharacterIndex(String itemInstanceId,
                                           Integer newCharacterIndex) {

        ContentValues contentValues = new ContentValues(1);
        contentValues.put(InventoryContract.InventoryEntry.COLUMN_CHARACTER_INDEX, newCharacterIndex);

        sqLiteDatabase.update(InventoryContract.InventoryEntry.TABLE_NAME,
                contentValues,
                InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_INSTANCE_ID + " = ?",
                new String[]{itemInstanceId});
    }

    public Optional<LocalInventoryItem> getCurrentlyEquippedItem(String itemCategory,
                                                                 int characterIndex) {

        String whereClause = InventoryContract.InventoryEntry.COLUMN_CHARACTER_INDEX + " = " +
                characterIndex + " AND " + InventoryContract.InventoryEntry.COLUMN_EQUIPPED +
                " = 1 AND " + InventoryContract.InventoryEntry.COLUMN_ITEM_CATEGORY + " = ?";

        FirebaseCrash.logcat(Log.DEBUG,
                TAG,
                String.format("getCurrentlyEquippedItem query [%s] category [%s]", whereClause, itemCategory));


        try (Cursor cursor = sqLiteDatabase.query(InventoryContract.InventoryEntry.TABLE_NAME,
                null,
                whereClause,
                new String[]{itemCategory},
                null,
                null,
                null)) {

            if (cursor.moveToNext()) {
                return Optional.of(createItem(cursor));
            }
        }

        return Optional.absent();
    }

    public void updateWeaponEquippedStatus(String itemInstanceId,
                                           String previousItemInstanceId) {

        ContentValues setEquippedValues = new ContentValues(1);
        setEquippedValues.put(InventoryContract.InventoryEntry.COLUMN_EQUIPPED, true);

        sqLiteDatabase.update(InventoryContract.InventoryEntry.TABLE_NAME,
                setEquippedValues,
                InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_INSTANCE_ID + " = ?",
                new String[]{itemInstanceId});

        ContentValues setUnequippedValues = new ContentValues(1);
        setUnequippedValues.put(InventoryContract.InventoryEntry.COLUMN_EQUIPPED, false);

        if (!Strings.isNullOrEmpty(previousItemInstanceId)) {
            sqLiteDatabase.update(InventoryContract.InventoryEntry.TABLE_NAME,
                    setUnequippedValues,
                    InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_INSTANCE_ID + " = ?",
                    new String[]{previousItemInstanceId});
        }
    }

    private LocalInventoryItem createItem(Cursor cursor) {

        LocalInventoryItem item = new LocalInventoryItem();
        item.setInstanceId(cursor.getString(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_INSTANCE_ID)));
        item.setHashId(cursor.getString(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_HASH)));
        item.setName(cursor.getString(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_NAME)));
        item.setLight(cursor.getInt(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_NAME_LIGHT)));
        item.setItemTypeName(cursor.getString(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_ITEM_TYPE_NAME)));
        item.setItemType(cursor.getString(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_ITEM_CATEGORY)));
        item.setCharacterIndex(cursor.getInt(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_CHARACTER_INDEX)));
        item.setItemClassType(ItemClassType.fromString(cursor.getString(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_ITEM_CLASS_TYPE))));
        item.setEquipped(1 == cursor.getInt(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_EQUIPPED)));
        return item;
    }
}

