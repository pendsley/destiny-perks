package com.pendsley.destinyperks.persistence.dao;

/**
 * Tables and columns from the Destiny manifest.
 *
 * @author Phil Endsley
 */
public class ManifestTables {

    private ManifestTables() {
    }

    public static final String DESTINY_INVENTORY_ITEM_DEFINITION_TABLE_NAME = "DestinyInventoryItemDefinition";
    public static final String DESTINY_TALENT_GRID_DEFINITION = "DestinyTalentGridDefinition";
    public static final String DESTINY_STAT_DEFINITION = "DestinyStatDefinition";
    public static final String DESTINY_CLASS_DEFINITION = "DestinyClassDefinition";

    public static final String COLUMN_NAME_ID = "id";
    public static final String COLUMN_NAME_JSON = "json";
}
