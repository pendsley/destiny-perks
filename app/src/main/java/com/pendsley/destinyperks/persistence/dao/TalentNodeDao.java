package com.pendsley.destinyperks.persistence.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.common.collect.Lists;
import com.pendsley.destinyperks.inject.DatabaseModule;
import com.pendsley.destinyperks.model.TalentNodeStep;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Database operations for talent nodes.
 *
 * @author Phil Endsley
 */
public class TalentNodeDao {

    private final SQLiteDatabase sqLiteDatabase;

    @Inject
    TalentNodeDao(@Named(DatabaseModule.WRITABLE_DATABASE) SQLiteDatabase sqLiteDatabase) {
        this.sqLiteDatabase = sqLiteDatabase;
    }

    public void deleteTalentNodes(Collection<String> itemIds) {

        sqLiteDatabase.delete(TalentNodeContract.TalentNodeEntry.TABLE_NAME,
                TalentNodeContract.TalentNodeEntry.COLUMN_NAME_ITEM_INSTANCE_ID + " IN " + generateArguments(itemIds.size()),
                itemIds.toArray(new String[itemIds.size()]));

    }

    public void batchInsertTalentNodes(Map<String, List<TalentNodeStep>> talentNodesMap) {

        sqLiteDatabase.beginTransaction();
        for (Map.Entry<String, List<TalentNodeStep>> entry : talentNodesMap.entrySet()) {
            for (TalentNodeStep talentNodeStep : entry.getValue()) {

                ContentValues contentValues = new ContentValues(5);
                contentValues.put(TalentNodeContract.TalentNodeEntry.COLUMN_NAME_ITEM_INSTANCE_ID, entry.getKey());
                contentValues.put(TalentNodeContract.TalentNodeEntry.COLUMN_NAME_NODE_STEP_NAME, talentNodeStep.getNodeStepName());
                contentValues.put(TalentNodeContract.TalentNodeEntry.COLUMN_NAME_NODE_STEP_DESCRIPTION, talentNodeStep.getNodeStepDescription());
                contentValues.put(TalentNodeContract.TalentNodeEntry.COLUMN_NAME_NODE_ICON, talentNodeStep.getIcon());
                contentValues.put(TalentNodeContract.TalentNodeEntry.COLUMN_NAME_STEP_INDEX, talentNodeStep.getStepIndex());
                contentValues.put(TalentNodeContract.TalentNodeEntry.COLUMN_ROW, talentNodeStep.getRow());
                contentValues.put(TalentNodeContract.TalentNodeEntry.COLUMN_COLUMN, talentNodeStep.getColumn());


                sqLiteDatabase.insert(TalentNodeContract.TalentNodeEntry.TABLE_NAME,
                        null,
                        contentValues);
            }
        }
        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
    }

    public List<TalentNodeStep> getTalentNodes(String itemInstanceId) {

        List<TalentNodeStep> talentNodeSteps = Lists.newArrayList();
        String whereClause = TalentNodeContract.TalentNodeEntry.COLUMN_NAME_ITEM_INSTANCE_ID + " = " + itemInstanceId;
        try (Cursor cursor = sqLiteDatabase.query(false,
                TalentNodeContract.TalentNodeEntry.TABLE_NAME,
                null,
                whereClause,
                null,
                null,
                null,
                null,
                null)) {

            while (cursor.moveToNext()) {
                TalentNodeStep talentNodeStep = new TalentNodeStep();
                talentNodeStep.setNodeStepName(cursor.getString(cursor.getColumnIndexOrThrow(TalentNodeContract.TalentNodeEntry.COLUMN_NAME_NODE_STEP_NAME)));
                talentNodeStep.setNodeStepDescription(cursor.getString(cursor.getColumnIndexOrThrow(TalentNodeContract.TalentNodeEntry.COLUMN_NAME_NODE_STEP_DESCRIPTION)));
                talentNodeStep.setIcon(cursor.getString(cursor.getColumnIndexOrThrow(TalentNodeContract.TalentNodeEntry.COLUMN_NAME_NODE_ICON)));
                talentNodeStep.setStepIndex(cursor.getInt(cursor.getColumnIndexOrThrow(TalentNodeContract.TalentNodeEntry.COLUMN_NAME_STEP_INDEX)));
                talentNodeStep.setRow(cursor.getInt(cursor.getColumnIndexOrThrow(TalentNodeContract.TalentNodeEntry.COLUMN_ROW)));
                talentNodeStep.setColumn(cursor.getInt(cursor.getColumnIndexOrThrow(TalentNodeContract.TalentNodeEntry.COLUMN_COLUMN)));

                talentNodeSteps.add(talentNodeStep);
            }
        }

        return talentNodeSteps;
    }


    private static String generateArguments(int size) {
        StringBuilder arguments = new StringBuilder("(");
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                arguments.append(",");
            }
            arguments.append("?");
        }
        arguments.append(")");

        return arguments.toString();
    }
}
