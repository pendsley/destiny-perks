package com.pendsley.destinyperks.presenter;

import com.google.common.collect.Lists;
import com.pendsley.destinyperks.R;
import com.pendsley.destinyperks.inject.AccountModule;
import com.pendsley.destinyperks.model.Character;
import com.pendsley.destinyperks.model.EquipOperationResult;
import com.pendsley.destinyperks.model.LocalInventoryItem;
import com.pendsley.destinyperks.model.VaultDestination;
import com.pendsley.destinyperks.operation.EquipItemOperation;
import com.pendsley.destinyperks.operation.TransferItemOperation;
import com.pendsley.destinyperks.persistence.datastore.InventoryCache;
import com.pendsley.destinyperks.viewcontroller.InventoryItemDetailsViewController;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Presenter for an inventory item's details.
 *
 * @author Phil Endsley
 */
public class InventoryItemDetailsPresenter {

    private final TransferItemOperation transferItemOperation;
    private final EquipItemOperation equipItemOperation;
    private final InventoryCache inventoryCache;
    private final List<Character> characters;
    private final CompositeDisposable disposables;

    private InventoryItemDetailsViewController inventoryItemDetailsViewController;
    private LocalInventoryItem item;

    @Inject
    InventoryItemDetailsPresenter(TransferItemOperation transferItemOperation,
                                  EquipItemOperation equipItemOperation,
                                  InventoryCache inventoryCache,
                                  @Named(AccountModule.CHARACTERS) List<Character> characters) {
        this.transferItemOperation = transferItemOperation;
        this.equipItemOperation = equipItemOperation;
        this.inventoryCache = inventoryCache;
        this.characters = characters;
        disposables = new CompositeDisposable();
    }

    public void setInventoryItemDetailsViewController(InventoryItemDetailsViewController inventoryItemDetailsViewController) {
        this.inventoryItemDetailsViewController = inventoryItemDetailsViewController;
    }

    public void setInventoryItem(LocalInventoryItem item) {
        this.item = item;
    }

    public void loadItemDetails() {
        inventoryItemDetailsViewController.displayItemTalentNodes(item.getTalentNodeSteps());
        inventoryItemDetailsViewController.displayItemStats(item.getStats());
    }

    public void loadTransferEquipCharacterOptions() {
        List<Character> sortedCharacters = Lists.newArrayList(characters);
        Collections.sort(sortedCharacters, new Comparator<Character>() {
            @Override
            public int compare(Character character1, Character character2) {
                return character1.getCharacterId().compareTo(character2.getCharacterId());
            }
        });

        sortedCharacters.add(new VaultDestination());

        inventoryItemDetailsViewController.setTransferEquipCharacterOptions(sortedCharacters,
                item.getCharacterIndex());
    }

    public void transferItem(Character destinationCharacter) {
        if (item.getCharacterIndex() < 0 && destinationCharacter instanceof VaultDestination) {
            displayErrorMessage(inventoryItemDetailsViewController.getContext()
                    .getString(R.string.item_already_in_vault));
            return;
        }

        Flowable<LocalInventoryItem> flowable =
                transferItemOperation.transferItem(destinationCharacter, item)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
        disposables.add(flowable.subscribeWith(new TransferItemSubscriber()));

        inventoryItemDetailsViewController.hideOptionsSheet();
    }

    public void equipItem(Character destinationCharacter) {

        String destinationCharacterId = destinationCharacter.getCharacterId();

        if (item.getCharacterIndex() < 0 && destinationCharacter instanceof VaultDestination) {
            displayErrorMessage(inventoryItemDetailsViewController.getContext()
                    .getString(R.string.item_already_in_vault, item.getName()));
            return;
        } else if (item.isEquipped()) {
            if (destinationCharacter.equals(characters.get(item.getCharacterIndex()))) {
                displayErrorMessage(inventoryItemDetailsViewController.getContext()
                        .getString(R.string.item_already_equipped, item.getName()));
            } else {
                displayErrorMessage(inventoryItemDetailsViewController.getContext()
                        .getString(R.string.item_currently_equipped, item.getName()));
            }
        }

        Flowable<EquipOperationResult> flowable =
                equipItemOperation.equipItem(item, destinationCharacterId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        disposables.add(flowable.subscribeWith(new EquipItemSubscriber()));

        inventoryItemDetailsViewController.hideOptionsSheet();
    }

    public void dispose() {
        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }

    private void displayInformationMessage(String message) {
        inventoryItemDetailsViewController.showInformationMessage(message);
    }

    private void displayErrorMessage(String errorMessage) {
        inventoryItemDetailsViewController.showErrorMessage(errorMessage);
    }

    private void transferFinished(LocalInventoryItem updatedItem) {
        inventoryCache.updateItem(updatedItem);

        displayInformationMessage(inventoryItemDetailsViewController.getContext()
                .getString(R.string.item_transferred, item.getName()));

        inventoryItemDetailsViewController.leaveDetailsView();
    }

    private void equipFinished(EquipOperationResult equipOperationResult) {
        LocalInventoryItem newItem = equipOperationResult.getNewItem();
        inventoryCache.updateItem(equipOperationResult.getPreviousItem());
        inventoryCache.updateItem(newItem);

        displayInformationMessage(inventoryItemDetailsViewController.getContext()
                .getString(R.string.item_equipped, newItem.getName()));

        inventoryItemDetailsViewController.leaveDetailsView();
    }

    private class TransferItemSubscriber extends DisposableSubscriber<LocalInventoryItem> {

        @Override
        public void onNext(LocalInventoryItem item) {
            transferFinished(item);
        }

        @Override
        public void onError(Throwable e) {
            displayErrorMessage(e.getLocalizedMessage());
        }

        @Override
        public void onComplete() {

        }
    }

    private class EquipItemSubscriber extends DisposableSubscriber<EquipOperationResult> {

        @Override
        public void onNext(EquipOperationResult equipOperationResult) {
            equipFinished(equipOperationResult);
        }

        @Override
        public void onError(Throwable e) {
            displayErrorMessage(e.getLocalizedMessage());
        }

        @Override
        public void onComplete() {

        }
    }
}
