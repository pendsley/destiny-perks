package com.pendsley.destinyperks.presenter;

import android.content.Context;

import com.google.common.collect.Lists;
import com.pendsley.destinyperks.R;
import com.pendsley.destinyperks.inject.AccountModule;
import com.pendsley.destinyperks.model.Character;
import com.pendsley.destinyperks.model.CharacterLoadoutsDrawerItem;
import com.pendsley.destinyperks.model.DrawerItem;
import com.pendsley.destinyperks.model.ItemClassType;
import com.pendsley.destinyperks.viewcontroller.InventoryDrawerViewController;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Presenter for the navigation drawer.
 *
 * @author Phil Endsley
 */
public class InventoryListDrawerPresenter {

    private final List<Character> characters;

    private InventoryDrawerViewController inventoryDrawerViewController;

    @Inject
    InventoryListDrawerPresenter(@Named(AccountModule.CHARACTERS) List<Character> characters) {
        this.characters = characters;
    }

    public void setInventoryDrawerViewController(InventoryDrawerViewController inventoryDrawerViewController) {
        this.inventoryDrawerViewController = inventoryDrawerViewController;
    }

    public void syncDrawerState() {
        inventoryDrawerViewController.syncDrawerState();
    }

    public void initializeInventoryDrawer() {

        Context context = inventoryDrawerViewController.getContext();

        List<DrawerItem> drawerItems = Lists.newArrayList();
        drawerItems.add(new DrawerItem(context.getString(R.string.category_all)));
        drawerItems.add(new DrawerItem(context.getString(R.string.category_weapons)));
        drawerItems.add(new DrawerItem(context.getString(R.string.category_armor)));
        for (Character character : characters) {
            drawerItems.add(new CharacterLoadoutsDrawerItem(character));
        }

        inventoryDrawerViewController.initializeDrawerItems(drawerItems);
    }

    public void drawerItemSelected(DrawerItem selectedDrawerItem) {
        inventoryDrawerViewController.closeDrawer();

        if (selectedDrawerItem instanceof CharacterLoadoutsDrawerItem) {
            inventoryDrawerViewController.navigateToLoadoutsList(((CharacterLoadoutsDrawerItem) selectedDrawerItem).getCharacter().getCharacterId());
        } else {
            inventoryDrawerViewController.navigateToInventoryList(ItemClassType.fromString(selectedDrawerItem.getScreenName()));
        }
    }

}
