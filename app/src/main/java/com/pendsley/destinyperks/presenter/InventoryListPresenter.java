package com.pendsley.destinyperks.presenter;

import android.content.Context;
import android.util.Log;

import com.google.common.collect.Lists;
import com.pendsley.destinyperks.R;
import com.pendsley.destinyperks.inject.AccountModule;
import com.pendsley.destinyperks.model.Character;
import com.pendsley.destinyperks.model.DestinyAssets;
import com.pendsley.destinyperks.model.InventoryFilterOption;
import com.pendsley.destinyperks.model.ItemClassType;
import com.pendsley.destinyperks.model.LocalInventoryItem;
import com.pendsley.destinyperks.operation.InventorySyncOperation;
import com.pendsley.destinyperks.persistence.dao.AccountOperation;
import com.pendsley.destinyperks.persistence.datastore.InventoryCache;
import com.pendsley.destinyperks.viewcontroller.InventoryListViewController;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Presenter for the list of inventory items.
 *
 * @author Phil Endsley
 */
public class InventoryListPresenter {

    private final InventoryCache inventoryCache;
    private final AccountOperation accountOperation;
    private final InventorySyncOperation inventorySyncOperation;
    private final List<Character> characters;
    private final CompositeDisposable disposables;

    private InventoryListViewController inventoryListViewController;

    @Inject
    InventoryListPresenter(InventoryCache inventoryCache,
                           AccountOperation accountOperation,
                           InventorySyncOperation inventorySyncOperation,
                           @Named(AccountModule.CHARACTERS) List<Character> characters) {
        this.inventoryCache = inventoryCache;
        this.accountOperation = accountOperation;
        this.inventorySyncOperation = inventorySyncOperation;
        this.characters = characters;
        disposables = new CompositeDisposable();
    }

    public void setInventoryListViewController(InventoryListViewController inventoryListViewController) {
        this.inventoryListViewController = inventoryListViewController;
    }


    public void syncInventoryItemsAndLoadByClassType(final ItemClassType itemClassType) {
        inventoryListViewController.showLoadingIndicator();

        Flowable<List<LocalInventoryItem>> flowable = inventorySyncOperation.syncCharactersAndInventory()
                .andThen(inventoryCache.getInventoryListFlowableByClassType(itemClassType))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        disposables.add(flowable.subscribeWith(new InventoryListItemsSubscriber()));
    }

    public void loadInventoryListByClassType(ItemClassType itemClassType) {
        inventoryListViewController.showLoadingIndicator();

        Flowable<List<LocalInventoryItem>> flowable =
                inventoryCache.getInventoryListFlowableByClassType(itemClassType)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
        disposables.add(flowable.subscribeWith(new InventoryListItemsSubscriber()));

    }

    public void syncInventoryItemsAndLoadByCategory(final String itemCategory) {
        inventoryListViewController.showLoadingIndicator();

        Flowable<List<LocalInventoryItem>> flowable = inventorySyncOperation.syncCharactersAndInventory()
                .andThen(inventoryCache.getInventoryListFlowableByCategory(itemCategory))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        disposables.add(flowable.subscribeWith(new InventoryListItemsSubscriber()));
    }

    public void loadInventoryListByCategory(String itemCategory) {
        inventoryListViewController.showLoadingIndicator();

        Flowable<List<LocalInventoryItem>> flowable = inventoryCache.getInventoryListFlowableByCategory(itemCategory)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        disposables.add(flowable.subscribeWith(new InventoryListItemsSubscriber()));
    }

    public List<String> loadCharacterEmblemPaths() {
        return accountOperation.getCharacterEmblemPaths();
    }

    public List<InventoryFilterOption> loadFilterOptions() {
        List<InventoryFilterOption> filterOptions = Lists.newArrayList();

        Context context = inventoryListViewController.context();

        InventoryFilterOption vaultOption = new InventoryFilterOption();
        vaultOption.setDescription(context.getString(R.string.vault));
        vaultOption.setCharacterIndex(-1);

        InventoryFilterOption allOption = new InventoryFilterOption();
        allOption.setEmblemBackgroundPath(DestinyAssets.ALL_FILTER_OPTION_BACKGROUND);
        allOption.setDescription(context.getString(R.string.all));
        allOption.setCharacterIndex(4);

        filterOptions.add(allOption);

        List<Character> sortedCharacters = Lists.newArrayList(characters);
        Collections.sort(sortedCharacters, new Comparator<Character>() {
            @Override
            public int compare(Character character1, Character character2) {
                return character1.getCharacterId().compareTo(character2.getCharacterId());
            }
        });

        for (Character character : sortedCharacters) {
            InventoryFilterOption filterOption = new InventoryFilterOption();
            filterOption.setEmblemBackgroundPath(character.getBackgroundPath());
            filterOption.setDescription(character.getCharacterClass().getClassName());
            filterOption.setCharacterIndex(character.getCharacterIndex());
            filterOptions.add(filterOption);
        }

        filterOptions.add(vaultOption);

        return filterOptions;
    }

    public void dispose() {
        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }

    private void handleInventoryFinishedLoading(List<LocalInventoryItem> inventoryItems) {
        showInventoryList(inventoryItems);
    }

    private void showInventoryList(List<LocalInventoryItem> inventoryItems) {
        inventoryListViewController.hideLoadingIndicator();
        inventoryListViewController.displayInventoryList(inventoryItems);
    }

    private final class InventoryListItemsSubscriber extends DisposableSubscriber<List<LocalInventoryItem>> {

        @Override
        public void onNext(List<LocalInventoryItem> localInventoryItems) {
            handleInventoryFinishedLoading(localInventoryItems);
        }

        @Override
        public void onError(Throwable e) {
            Log.e("", "Error getting inventory list", e);
        }

        @Override
        public void onComplete() {

        }
    }

}
