package com.pendsley.destinyperks.presenter;

import com.pendsley.destinyperks.model.ItemSlot;
import com.pendsley.destinyperks.model.Loadout;
import com.pendsley.destinyperks.model.LocalInventoryItem;
import com.pendsley.destinyperks.model.ValidationException;
import com.pendsley.destinyperks.operation.LoadoutDetailsOperation;
import com.pendsley.destinyperks.persistence.datastore.LoadoutEditingCache;
import com.pendsley.destinyperks.viewcontroller.LoadoutDetailsViewController;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Presenter for the details and items of a loadout.
 *
 * @author Phil Endsley
 */
public class LoadoutDetailsPresenter {

    private final LoadoutEditingCache loadoutEditingCache;
    private final LoadoutDetailsOperation loadoutDetailsOperation;

    private final CompositeDisposable disposables;

    private LoadoutDetailsViewController loadoutDetailsViewController;

    @Inject
    LoadoutDetailsPresenter(LoadoutEditingCache loadoutEditingCache,
                            LoadoutDetailsOperation loadoutDetailsOperation) {
        this.loadoutEditingCache = loadoutEditingCache;
        this.loadoutDetailsOperation = loadoutDetailsOperation;

        disposables = new CompositeDisposable();
    }

    public void setLoadoutDetailsViewController(LoadoutDetailsViewController loadoutDetailsViewController) {
        this.loadoutDetailsViewController = loadoutDetailsViewController;
    }

    public void loadLoadout() {

        Flowable<List<LocalInventoryItem>> flowable =
                loadoutDetailsOperation.getLoadoutItems(loadoutEditingCache.getLoadout())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
        disposables.add(flowable.subscribeWith(new LoadLoadoutSubscriber()));
    }

    public void clearLoadoutSlot(ItemSlot slot) {
        loadoutEditingCache.clearLoadoutSlot(slot);
        loadoutDetailsViewController.clearLoadoutItemSlot(slot);
    }

    public void saveLoadout() {
        try {
            Completable completable = loadoutDetailsOperation.saveLoadout(loadoutEditingCache.getLoadout())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
            disposables.add(completable.subscribeWith(new SaveLoadoutObserver()));

        } catch (ValidationException e) {
            loadoutDetailsViewController.showErrorMessage(e.getMessageResource());
        }
    }

    public void promptLoadoutDeletion() {
        loadoutDetailsViewController.showDeleteLoadoutDialog();
    }

    public void deleteLoadout() {
        Loadout loadout = loadoutEditingCache.getLoadout();

        if (loadout.getLoadoutId() > 0) {
            loadoutDetailsOperation.deleteLoadout(loadout.getLoadoutId());
        }

        finishLoadoutDetails();
    }

    public void dispose() {
        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }

    private void saveLoadoutFinished() {
        finishLoadoutDetails();
    }

    private void finishLoadoutDetails() {
        loadoutEditingCache.expireCache();
        loadoutDetailsViewController.leaveLoadoutDetails();
    }

    private void loadoutFinishedLoading(List<LocalInventoryItem> loadoutItems) {

        loadoutDetailsViewController.displayLoadout(loadoutEditingCache.getLoadout(), loadoutItems);
    }

    private class LoadLoadoutSubscriber extends DisposableSubscriber<List<LocalInventoryItem>> {

        @Override
        public void onNext(List<LocalInventoryItem> loadoutItems) {
            loadoutFinishedLoading(loadoutItems);
        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onComplete() {

        }
    }

    private class SaveLoadoutObserver extends DisposableCompletableObserver {

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onComplete() {
            saveLoadoutFinished();
        }
    }
}
