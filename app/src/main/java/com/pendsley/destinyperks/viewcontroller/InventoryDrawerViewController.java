package com.pendsley.destinyperks.viewcontroller;

import android.content.Context;

import com.pendsley.destinyperks.model.DrawerItem;
import com.pendsley.destinyperks.model.ItemClassType;

import java.util.List;

/**
 * Inventory navigation drawer view.
 *
 * @author Phil Endsley
 */
public interface InventoryDrawerViewController {

    void syncDrawerState();

    void closeDrawer();

    void initializeDrawerItems(List<DrawerItem> drawerItems);

    void navigateToInventoryList(ItemClassType itemClassType);

    void navigateToLoadoutsList(String characterId);

    Context getContext();
}
