package com.pendsley.destinyperks.viewcontroller;

import android.content.Context;

import com.pendsley.destinyperks.model.Character;
import com.pendsley.destinyperks.model.Stat;
import com.pendsley.destinyperks.model.TalentNodeStep;

import java.util.List;

/**
 * View for an inventory item's details.
 *
 * @author Phil Endsley
 */
public interface InventoryItemDetailsViewController {

    void displayItemTalentNodes(List<TalentNodeStep> talentNodeSteps);

    void displayItemStats(List<Stat> stats);

    void setTransferEquipCharacterOptions(List<Character> characters,
                                          int itemCharacterIndex);

    void hideOptionsSheet();

    void showInformationMessage(String message);

    void showErrorMessage(String errorMessage);

    void leaveDetailsView();

    Context getContext();
}
