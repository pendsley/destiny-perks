package com.pendsley.destinyperks.viewcontroller;

import com.pendsley.destinyperks.model.ItemSlot;
import com.pendsley.destinyperks.model.Loadout;
import com.pendsley.destinyperks.model.LocalInventoryItem;

import java.util.List;
import java.util.Map;

/**
 * View for displaying a loadout's details and items.
 *
 * @author Phil Endsley
 */
public interface LoadoutDetailsViewController {

    void displayLoadout(Loadout loadout,
                        List<LocalInventoryItem> loadoutItems);

    void clearLoadoutItemSlot(ItemSlot itemSlot);

    void showDeleteLoadoutDialog();

    void showErrorMessage(int errorMessageResource);

    void leaveLoadoutDetails();
}
