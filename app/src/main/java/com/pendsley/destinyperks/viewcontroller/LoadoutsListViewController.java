package com.pendsley.destinyperks.viewcontroller;

import com.pendsley.destinyperks.model.Loadout;

import java.util.List;

/**
 * View for displaying a list of loadouts.
 *
 * @author Phil Endsley
 */
public interface LoadoutsListViewController {

    void displayLoadouts(List<Loadout> loadouts);

    void showLoadoutDetails(Loadout loadout);

    void loadoutEquipped(Loadout loadout,
                         List<Throwable> errors);

}
